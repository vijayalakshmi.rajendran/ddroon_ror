# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron_dev.log"
#
every 1.minutes do
# every 1.day, :at => '12:05 am' do	
#   command "/usr/bin/some_great_command"
   runner "User.db_backup"
   # rake "sudo mongodump --db=ddroon_prod --username ddroon_live --password ddroon_live --gzip --out ../backup"
end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

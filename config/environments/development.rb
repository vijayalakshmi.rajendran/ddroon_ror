Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false

  # config.hosts << "873213c5.ngrok.io"
  config.hosts << "ddroon-api.altiussolution.com" 

  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    config.cache_store = :null_store
  end


  ENV['DDROON_URL'] = "http://3.218.187.175"

  ENV['SUPERADMIN'] = "SUPERADMIN"
  ENV['ADMIN'] = "ADMIN"
  ENV['CONTENT PRODUCER'] = "CONTENT PRODUCER"

  ENV["FCM_SERVER_KEY"] = "AAAAMCX7w7w:APA91bEPjr5BWHzCt13zLJ2YlcLA3ODiljxh4tNLnPIohcYNvEljvU20GD39_ZTz80uebJeNbbOR-wWKijibJMLBGP5yGWEwZQsFUBzm5egWLUbBFoRPKwCXbZLuADWrFX7lhen5IF-s"
  
  # ENV["from_mail"] = "altius.ddroon@gmail.com"
  ENV["from_mail"] = "ddroonaltius@gmail.com"
  ENV["cc_mail"] = "vijayalakshmi.rajendran@adcltech.com"
  ENV["bcc_mail"] = "vijayalakshmi.rajendran@adcltech.com"
  #mails configuration
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address:              'smtp.gmail.com',
    port:                 587,
    domain:               'example.com',
    user_name:            'ddroonaltius@gmail.com', # 'altius.ddroon@gmail.com',
    password:             'ddroon@2020', # '@ltius@123',
    authentication:       'plain',
    enable_starttls_auto: true
  }

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log


  # Raises error for missing translations.
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker
end

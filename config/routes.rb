Rails.application.routes.draw do

  get 'user_log_details' => 'user_logs#user_log_details'
  get 'log_users' => 'filters#log_users'
  get 'log_checkout_time' => 'user_logs#log_checkout_time'

  get 'repository_file_details/:id' => 'repository_file_datas#repository_file_details'
  post 'update_input_headers' => 'project_files#update_input_headers'
  post 'archive_file' => 'project_files#archive_file'
  get 'repository_file_datas' => 'repository_file_datas#index'

  get 'move_taxonomy_to_repository' => 'repository_files#move_taxonomy_to_repository'
  get 'move_schema_to_repository' => 'repository_files#move_schema_to_repository'

  get 'download_schema_sheet' => 'download_sheets#download_schema_sheet'
  get 'download_taxonomy_sheet' => 'download_sheets#download_taxonomy_sheet'
  get 'download_sku_sheet' => 'download_sheets#download_sku_sheet'

  post 'view_user_worksheets' => 'view_user_worksheets#index'

  get 'validate_repo_filename' => 'validations#validate_repo_filename'
  get 'validate_repository_name' => 'validations#validate_repository_name'
  get 'repository_projects' => 'repository_files#repository_projects'

  get 'restore_file/:id' => 'project_files#restore_file'
  get 'trashed_files' => 'project_files#trashed_files'
  get 'restore_project/:id' => 'projects#restore_project'
  get 'trashed_projects' => 'projects#trashed_projects'

  post 'multiple_schema_allocation' => 'schema_allocations#multiple_schema_allocation'
  post 'multiple_taxonomy_allocation' => 'taxonomy_allocations#multiple_taxonomy_allocation'
  post 'multiple_allocation' => 'allocations#multiple_allocation'

  get 'overall_taxonomy_files' => 'overall_taxonomy_files#index'
  get 'overall_schema_files' => 'overall_schema_files#index'
  post 'pull_schema_worksheets' => 'pull_schema_worksheets#pull_schema_data'
  post 'pull_taxonomy_worksheets' => 'pull_taxonomy_worksheets#pull_taxonomy_data'
  post 'pull_sku_worksheets' => 'pull_sku_worksheets#pull_sku_data'

  get 'allocated_users' => 'filters#allocated_users'

  get 'get_schema_column_values' => 'filters#get_schema_column_values'
  get 'get_taxonomy_column_values' => 'filters#get_taxonomy_column_values'
  get 'get_sku_column_values' => 'filters#get_sku_column_values'

  get 'schema_worksheets' => 'schema_worksheets#send_sku_data'
  post 'schema_worksheets' => 'schema_worksheets#save_sku_data'

  post 'schema_sheets' => 'schema_sheets#save_sku_data'
  get 'schema_sheets' => 'schema_sheets#send_data_to_view'

  get 'schema_sheet_allocations' => 'schema_sheet_allocations#send_data_for_allocation'
  post 'schema_sheet_allocations' => 'schema_sheet_allocations#save_allocation_data'
  get 'schema_allocations' => 'schema_allocations#index'
  post 'schema_allocations' => 'schema_allocations#create'

  get 'taxonomy_worksheets' => 'taxonomy_worksheets#send_sku_data'
  post 'taxonomy_worksheets' => 'taxonomy_worksheets#save_sku_data'

  post 'taxonomy_sheets' => 'taxonomy_sheets#save_sku_data'
  get 'taxonomy_sheets' => 'taxonomy_sheets#send_data_to_view'

  get 'taxonomy_sheet_allocations' => 'taxonomy_sheet_allocations#send_data_for_allocation'
  post 'taxonomy_sheet_allocations' => 'taxonomy_sheet_allocations#save_allocation_data'
  get 'taxonomy_allocations' => 'taxonomy_allocations#index'
  post 'taxonomy_allocations' => 'taxonomy_allocations#create'

  get 'schema_files' => 'schemas#schema_files'
  get 'schema_details' => 'schemas#schema_details'
  get 'schemas_pivot' => 'schemas#pivot_view'

  post 'sku_sheets' => 'sku_sheets#save_sku_data'
  get 'sku_sheets' => 'sku_sheets#send_data_to_view'

  get 'taxonomy_files' => 'taxonomies#taxonomy_files'
  get 'taxonomy_details' => 'taxonomies#taxonomy_details'
  get 'taxonomies_pivot' => 'taxonomies#pivot_view'

  get 'validate_username' => 'validations#validate_username'
  get 'validate_email_id' => 'validations#validate_email_id'
  get 'validate_project_code' => 'validations#validate_project_code'
  get 'validate_project_name' => 'validations#validate_project_name'

  post 'reset_password' => 'authentication#reset_password'

  post 'forgot_password' => 'authentication#verify_code'
  get 'forgot_password' => 'authentication#verify_username'

  get 'content_producer_dashboard' => 'dashboard#content_producer_dashboard'
  get 'admin_dashboard' => 'dashboard#admin_dashboard'
  get 'processwise_data' => 'dashboard#processwise_data'
  get 'admin_card_detail' => 'dashboard#admin_card_detail'
  get 'completed_projects' => 'dashboard#completed_projects'

  get 'sku_worksheets' => 'sku_worksheets#send_sku_data'
  post 'sku_worksheets' => 'sku_worksheets#save_sku_data'

  get 'download_sku_worksheets' => 'download_sku_worksheets#download_sku_data'

  get 'sheet_allocations' => 'sheet_allocations#send_data_for_allocation'
  post 'sheet_allocations' => 'sheet_allocations#save_allocation_data'

  get 'sku_details' => 'skus#sku_details'
  get 'skus_pivot' => 'skus#pivot_view'

  resources :repository_files
  resources :schemas
  resources :taxonomies
  resources :allocations
  resources :skus
  resources :project_files
  resources :projects
  resources :services
  resources :content_processes
  resources :project_types
  resources :client_types
  resources :user_logs
  resources :regions
  resources :users
  resources :roles

  post '/auth/login', to: 'authentication#login'
  get '/*a', to: 'application#not_found'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

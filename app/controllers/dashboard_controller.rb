class DashboardController < ApplicationController
    include ContentProducerDashboard, AdminDashboard # To include the 'content_producer_dashboard', 'admin_dashboard' concern

	# GET - /content_producer_dashboard
	def content_producer_dashboard
		@response = projectwise_allocations # Logics are executed in 'content_producer_dashboard' concern 'projectwise_allocations' method
		render json: @response
	end

	# GET - /admin_dashboard
	def admin_dashboard
		@response = projects_summary # Logics are executed in 'admin_dashboard' concern 'projects_summary' method
		render json: @response
	end

	def processwise_data
		project = Project.find_by(db_name: params[:db_name]) if params[:db_name].present?
		if project.present?
			response = single_project_summary(project)
			render json: response
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end

	# GET - /admin_card_detail

    def admin_card_detail
		total_project_count = @current_user.is_QA_admin? ? Project.projects.count : Project.count
     	completed_project_count = @current_user.is_QA_admin? ? Project.projects.where(status: "COMPLETED").count : Project.where(status: "COMPLETED").count
		taxonomy_count_arr = []
		schema_count_arr = []
		
		if @current_user.is_QA_admin?
			Project.projects do |project|
				project.tenancy!
				taxonomy_file = FileProcessMapping.where(content_process_id: ContentProcess.find_by(name: "Taxonomy"))
				schema_file = FileProcessMapping.where(content_process_id: ContentProcess.find_by(name: "Schema"))
            	taxonomy_count_arr << taxonomy_file.count if taxonomy_file.present?
		    	schema_count_arr << schema_file.count if schema_file.present?
		    end
		else
			Project.all.map do |project|	
		    	project.tenancy!
		    	taxonomy_file = FileProcessMapping.where(content_process_id: ContentProcess.find_by(name: "Taxonomy"))
				schema_file = FileProcessMapping.where(content_process_id: ContentProcess.find_by(name: "Schema"))
            	taxonomy_count_arr << taxonomy_file.count if taxonomy_file.present?
		    	schema_count_arr << schema_file.count if schema_file.present?
			end
		end
        response_data = {total_projects: total_project_count, completed_projects: completed_project_count, taxonomy_files: taxonomy_count_arr.sum, schema_files: schema_count_arr.sum}
		render json: response_data
    end

    # GET - /completed_projects
	def completed_projects
		@response = completed_projects_summary # Logics are executed in 'completed_projects' concern 'completed_projects_summary' method
		render json: @response
	end

end



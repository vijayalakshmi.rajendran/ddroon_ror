require 'fcm'

class TaxonomyAllocationsController < ApplicationController

  # GET /taxonomy_allocations
  def index
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?
      @project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
      @taxonomies = Taxonomy.where(project_file_id: project_file.id)
      column_values = @taxonomies.pluck(params[:column_name]).uniq
      allocation_data = {}
      column_values.map do |i|
        @columwise_taxonomies = @taxonomies.where(params[:column_name] => i)
        allocated_taxonomies = @columwise_taxonomies.where(:allocated_to_id.ne => nil)
        allocated_taxonomy_data = allocated_taxonomies.present? ? allocated_taxonomies.map{|i| {sku_id: i.id, allocated_to: i.allocated_to}} : []
        unallocated_taxonomies = @columwise_taxonomies.present? && @columwise_taxonomies.where(allocated_to_id: nil).present? ? @columwise_taxonomies.where(allocated_to_id: nil).pluck(:_id) : []
        user_data = {}
        user_taxonomies = allocated_taxonomies.group_by{|i| i.allocated_to} if allocated_taxonomies.present?
        user_taxonomies.map{|k, v| user_data[k.username] = v.map{|i| i._id}} if user_taxonomies.present?
        allocation_data[i] = {allocated_skus: allocated_taxonomy_data, unallocated_skus: unallocated_taxonomies, user_data: user_data}
      end
      total_allocated_count = @taxonomies.where(:allocated_to_id.ne => nil).count
      render json: {allocation: allocation_data, allocated: total_allocated_count, unallocated: @taxonomies.count - total_allocated_count}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # POST /taxonomy_allocations
  def create
   	@project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?
      @project.tenancy!  
      # content_process = ContentProcess.find(params[:content_process_id]) if params[:content_process_id].present?
      @content_producer = User.find(params[:content_producer_id]) if params[:content_producer_id].present?
      content_producer_id = @content_producer.id if @content_producer.present?
      admin = User.find(params[:admin_id]) if params[:admin_id].present?
      project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
      begin
	      params[:ids].map do |i| 
	        taxonomy = Taxonomy.find(i) rescue nil
	        if taxonomy.present?
	           content_producer_id.present? ? taxonomy.update(allocated_to_id: content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id) : taxonomy.update(allocated_to_id: nil, allocated_by_id: nil, project_file_id: project_file.id)
	        end
	      end
	      	         
        if @content_producer.present? && @content_producer.fcm_token.present?
            fcm = FCM.new(ENV["FCM_SERVER_KEY"])
            registration_ids= [@content_producer.fcm_token] # an array of one or more client registration tokens
            options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
            fcm.send(registration_ids, options)
        end
	      render json: {status: true, message: "Allocated successfully"}, status: :ok
	    rescue
    	  render json: {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
      end  
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # POST /multiple_taxonomy_allocation
  def multiple_taxonomy_allocation
        @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?
      @project.tenancy!      
	registration_ids = []
#      content_producer_id = User.find(params[:content_producer_id]).id if params[:content_producer_id].present?
      admin = User.find(params[:admin_id]) if params[:admin_id].present?
      project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
      begin
          params[:allocation_data].map do |data|
	          data[:taxonomy_ids].map do |i|
                taxonomy = Taxonomy.find(i) rescue nil
                if taxonomy.present?
		              @content_producer = User.find(data[:content_producer_id]) if data[:content_producer_id].present?
            		  if @content_producer.present? && @content_producer.fcm_token.present?
                    registration_ids << @content_producer.fcm_token unless registration_ids.include?(@content_producer.fcm_token) # an array of one or more client registration tokens
                  end
                  data[:content_producer_id].present? ? taxonomy.update(allocated_to_id: @content_producer.id, allocated_by_id: admin.id, project_file_id: project_file.id) : taxonomy.update(allocated_to_id: nil, allocated_by_id: nil, project_file_id: project_file.id)
                end
	          end
          end
          if registration_ids != []
	          fcm = FCM.new(ENV["FCM_SERVER_KEY"])
            options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
            fcm.send(registration_ids, options)
          end
          render json: {status: true, message: "Allocated successfully"}, status: :ok
      rescue
          render json: {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
      end
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end



    # # GET /taxonomy_sheet
  # def taxonomy_sheet
  #     @project = Project.find_by(db_name: params[:db_name])

  #     if @project.present?
        
  #       @project.tenancy!

  #       # Authenticate a session with your Service Account
  #       session = GoogleDrive::Session.from_service_account_key("client_secret.json")
  #       # spreadsheet = session.spreadsheet_by_key("1JVK36aH0k_6_l04xmqKtC4l6yHlmgPJ64LS2-L9Z670")
  #       spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
  #       # Get the first worksheet
  #       worksheet = spreadsheet.worksheets.first

  #       project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?
  #       sheet_headers = project_file.actual_headers

  #       # For clearing the sheet values
  #       (1..worksheet.num_rows).each do |row|
  #           (1..worksheet.num_cols).each do |col|
  #         worksheet[row, col] = ""
  #           end
  #       end

  #       worksheet.insert_rows(1, [sheet_headers])
  #       worksheet.save

  #       @taxonomies = Taxonomy.where(project_file_id: project_file.id)

  #       sheet_data = []
  #       @taxonomies.map do |sku|
  #         taxonomy_data = sheet_headers.map{|i| sku[i]}
  #         sheet_data << taxonomy_data
  #       end

  #       worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
  #       worksheet.save

  #       render json: {status: true, message: "Data Sent Successfully"}
  #     else
  #       render json: {status: false, message: "Invalid Project Code"}
  #     end
  # end

end

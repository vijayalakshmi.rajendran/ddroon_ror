class SchemaWorksheetsController < ApplicationController

	include WorksheetLog # To include the worksheet_log concern 
	include UserProductionLog

	def send_sku_data
		@project = Project.find_by(db_name: params[:db_name])
		if @project.present?
			@project.tenancy!

			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?
			project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?

			@user_worksheet_log = UserWorksheetLog.find_by(sheet_id: params[:sheet_id]) rescue nil

			if @user_worksheet_log.present?
				render json: {status: false, message: "Sheet ID already have an unsaved data"}
			else
				# Authenticate a session with your Service Account
				session = GoogleDrive::Session.from_service_account_key("client_secret.json")
				# spreadsheet = session.spreadsheet_by_key("1JVK36aH0k_6_l04xmqKtC4l6yHlmgPJ64LS2-L9Z670")
				spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
				# Get the first worksheet
				worksheet = spreadsheet.worksheets.first

				sheet_headers = ["id", "status" ] 
				project_file.actual_headers.delete("status") if project_file.actual_headers.present?
				sheet_headers = sheet_headers + project_file.actual_headers if project_file.actual_headers.present?
				
				# For clearing the sheet values
				(1..worksheet.num_rows).each do |row|
				    (1..worksheet.num_cols).each do |col|
					worksheet[row, col] = ""
				    end
				end
				worksheet.insert_rows(1, [sheet_headers])
				worksheet.save

				@skus = Schema.where(project_file_id: project_file.id, :allocated_to_id => params[:content_producer_id])

				sheet_data = []
				@skus.map do |sku|
					sku_data = sheet_headers.map do |i|
					    sku[i]
					end
					sheet_data << sku_data
				end

				sheet_data.each_slice(1000) do |data|
					worksheet.insert_rows(worksheet.rows.count+1, data)
					worksheet.save
				end

				#worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
				#worksheet.save
	
				header_count =  project_file.input_headers.present? ? project_file.input_headers.count : 0

				# To hide the id field column in the sheet for end_user
				first_worksheet = spreadsheet.worksheets.first
				sheet_id = first_worksheet.sheet_id

				first_worksheet.add_request(
				  {
				    add_protected_range: {
				      protected_range: {
				        range: {
				          sheet_id: sheet_id,
				          start_column_index: 0,
				          end_column_index: 0,
				        },
				        description: 'Protecting total row',
				        warning_only: true,
				        requesting_user_can_edit: false,
				      }
				    }
				  }
				)

				first_worksheet.save

				first_worksheet.add_request(
				  {
				    add_protected_range: {
				      protected_range: {
				        range: {
				          sheet_id: sheet_id,
				          start_column_index: 2,
				          end_column_index: header_count+2,
				        },
				        description: 'Protecting total row',
				        warning_only: true,
				        requesting_user_can_edit: false,
				      }
				    }
				  }
				)

				first_worksheet.save

				# To protect the id field column cells from editing by mistake
				first_worksheet.add_request(
					{
					  	update_dimension_properties: {
						    range: {
						      sheet_id: sheet_id,
						      dimension: 'COLUMNS',
						      start_index: 0,
						      end_index: 1,
						    },
						    properties: {
						      hidden_by_user: true,
						    },
						    fields: 'hidden_by_user',
						}
					}
				)

				first_worksheet.save

				# To save the content producer worksheet activity
				create_worksheet_log(@project.id, project_file.id, content_process.id, @current_user.id, params[:sheet_id])

				render json: {status: true, message: "Data Sent Successfully"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end
	

	def save_sku_data
		@project = Project.find_by(db_name: params[:db_name])
		registration_ids = []
		production_ids = []
		if @project.present?
			@project.tenancy!
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			worksheet = spreadsheet.worksheets[1]
			if worksheet.present?
				sheet_data = worksheet.rows
				column_names = sheet_data[0]

				project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?
				content_process = ContentProcess.find_by(name: params[:content_process_name]) if params[:content_process_name].present?

				new_header_values = column_names - ["id"] if column_names.present?
	            project_file.update(actual_headers: new_header_values.uniq) if new_header_values.present?

	            ind = column_names.index("id")
			    schema_ids = sheet_data[1..-1].collect {|data| data[ind]}
			    @schemas = Schema.where(:id.in => schema_ids) if schema_ids.present?

				sheet_data[1..-1].map do |data|
					updatable_data = {}
					column_names.each_with_index do |col_name, index|
						unless project_file.input_headers.include? col_name
							updatable_data[col_name] = data[index]
						end
					end
					@schema = @schemas.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
					if @schema.present?
						if updatable_data["status"].present? && updatable_data["status"].downcase == "reallocate"
							updatable_data["allocated_to_id"] = nil
							if @schema.allocated_by.present? && @schema.allocated_by.fcm_token.present?
								registration_ids << @schema.allocated_by.fcm_token unless registration_ids.include?(@schema.allocated_by.fcm_token)
							end
						end

						if @schema.status != updatable_data["status"] && updatable_data["status"] != "" && updatable_data["status"].downcase != "reallocate"
							production_ids << @schema.id unless production_ids.include?(@schema.id)
						end

						@schema.update(updatable_data)
					end
				end

				if registration_ids != []
					fcm = FCM.new(ENV["FCM_SERVER_KEY"])
				    options = { "notification": {"title": "DDROON", "body": "Hi, #{@current_user.username} have reallocated some of the SKUs in the file #{project_file.file_name} of the project #{@project.name} for the process Schema"}}
				    fcm.send(registration_ids, options)
				end

				# For clearing the sheet values
				(1..worksheet.num_rows).each do |row|
				    (1..worksheet.num_cols).each do |col|
							worksheet[row, col] = ""
				    end
				end
				worksheet.save

				# To log the production count of a user
				update_production_info(@current_user.id, @project.name, production_ids, "Schema")

				# To delete the content producer worksheet log 
				delete_worksheet_log(@project.id, project_file.id, content_process.id, @current_user.id, params[:sheet_id])

				render json: {status: true, message: "Data saved Successfully"}
			else
				render json: {status: false, message: "No worksheet found to save"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end

end







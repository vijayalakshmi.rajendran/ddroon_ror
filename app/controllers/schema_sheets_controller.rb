class SchemaSheetsController < ApplicationController

	# GET - /schema_sheets
	def send_data_to_view
		@project = Project.find_by(db_name: params[:db_name]) rescue nil

		if @project.present?
			@project.tenancy!
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			# Get the first worksheet
			worksheet = spreadsheet.worksheets.first
			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?

			sheet_headers = ["id", "status" ] 
			project_file.actual_headers.delete("status") if project_file.actual_headers.present?
			sheet_headers = sheet_headers + project_file.actual_headers if project_file.actual_headers.present? # + ["status"]

			# sheet_headers = ["id"] + project_file.actual_headers
            sheet_headers << "allocated_to" unless sheet_headers.include?("allocated_to")
            
            # sheet_headers << "allocated_by" unless sheet_headers.include?("allocated_by")

			# Dumps all cells.
			(1..worksheet.num_rows).each do |row|
			  (1..worksheet.num_cols).each do |col|
			    worksheet[row, col] = ""
			  end
			end
			worksheet.insert_rows(1, [sheet_headers])
			worksheet.save

			@schemas = Schema.where(project_file_id: params[:file_id])
			sheet_data = []
			@schemas.map do |schema|			
				schema_data = []
                                sheet_headers.map do |i|
                                   if i == "allocated_to"
                                      user = User.find(schema.allocated_to_id) rescue nil
                                      username = user.present? ? user.username : ""
                                      schema_data << username
                                   elsif i == "allocated_by"
                                      user = User.find(schema.allocated_by_id) rescue nil
                                      username = user.present? ? user.username : ""
                                      schema_data << username
                                   else
                                      schema_data << schema[i]
                                   end
                                end
                                sheet_data << schema_data	
				#schema_data = sheet_headers.map{|i| schema[i]} 
				#sheet_data << schema_data
			end

			worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
			worksheet.save

			render json: {status: true, message: "Data Sent Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end

	# POST - /schema_sheets
	def save_sku_data
		@project = Project.find_by(db_name: params[:db_name])
		if @project.present?
			@project.tenancy!
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			worksheet = spreadsheet.worksheets.first
			sheet_data = worksheet.rows
			column_names = sheet_data[0]

			project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?
			content_process = ContentProcess.find_by(name: params[:content_process_name]) if params[:content_process_name].present?

			new_header_values = project_file.actual_headers - (column_names - ["id"])
			total_header_values = project_file.actual_headers + new_header_values
			project_file.update(actual_headers: total_header_values)

			sheet_data[1..-1].map do |data|
				updatable_data = {}
				column_names.each_with_index do |col_name, index|
					#unless project_file.input_headers.include?(col_name)
						updatable_data[col_name] = data[index]
					#end
				end
				@schema = Schema.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
				if @schema.present?
					@schema.update(updatable_data)
				end
			end

			# To delete the content producer worksheet activity when the activity gets completed
			sheet_logs = UserWorksheetLog.where(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:content_producer_id]) rescue nil if params[:content_producer_id].present?
      		sheet_logs.delete_all if sheet_logs.present?

			render json: {status: true, message: "Data saved Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end

end

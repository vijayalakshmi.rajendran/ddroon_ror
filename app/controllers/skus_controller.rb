class SkusController < ApplicationController
  before_action :set_sku, only: [:show, :update, :destroy]

  # GET /skus
  def index
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      
      if params[:search_key].present? && params[:search_key].length > 0
          @skus = Sku.full_text_search(params[:search_key], match: :all)
          @skus = @skus.where(project_file_id: params[:file_id]) if params[:file_id].present? && @skus.present?
      else
          @skus = Sku.where(project_file_id: params[:file_id])
      end
      @skus = @skus.order("#{params[:sort_key]} #{params[:sort_order]}") if params[:sort_key].present? && params[:sort_order].present?
      if params[:assigned].present?
        column_values = params[:column_values].split(".") if params[:column_values].present?
        sku_ids = @skus.pluck(:_id) 
        assigned_sku_ids = Allocation.where(:sku_id.in => sku_ids, :content_process_id => params[:content_process_id]).pluck(:sku_id)
        unassigned_sku_ids = sku_ids - assigned_sku_ids
        @skus = params[:assigned] == "true" || params[:assigned] == true ? @skus.in(params[:column_name] => column_values, id: assigned_sku_ids) : @skus.in(params[:column_name] => column_values, id: unassigned_sku_ids)
        @skus = @skus.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?     
        @skus = @skus.paginate(page: params[:page], per_page: params[:per_page])
        sku_data = []
        @skus.map do |sku|
	    allocation = sku.allocations.find_by(:content_process_id => params[:content_process_id]) rescue nil if sku.allocations.present?
            allocated_to = allocation.allocated_to if allocation.present?
            allocated_by = allocation.allocated_by if allocation.present?
            sku_data << {sku: sku, allocated_to: allocated_to, allocated_by: allocated_by}
        end
        render json: sku_data
      else
        render json: @skus.paginate(page: params[:page], per_page: params[:per_page])
      end
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET /skus_pivot
  def pivot_view
    project = Project.find_by(db_name: params[:db_name])
    if project.present?  
      project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
      @skus = Sku.where(project_file_id: project_file._id)  
      @pivot_data = {}
      column_names = params[:column_names].present? ? params[:column_names].split(".") : nil
      if column_names.present?
	column_names.map{|i| @pivot_data[i] = @skus.pluck(i).uniq}
      else
	project_file.actual_headers.map{|i| @pivot_data[i] = @skus.pluck(i).uniq}
      end
     
      render json: @pivot_data
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET /sku_details
  def sku_details
    project = Project.find_by(db_name: params[:db_name])
    if project.present?  
      project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
    
      if params[:search_key].present? && params[:search_key].length > 0
          @skus = Sku.full_text_search(params[:search_key], match: :all)
          @skus = @skus.where(project_file_id: project_file._id) if @skus.present? && project_file.present?
      else
          @skus = Sku.where(project_file_id: project_file._id)
      end
      column_values = params[:column_values].split(".") if params[:column_values].present?
      
      @skus = @skus.in(params[:column_name] => column_values) if params[:column_name].present? && column_values.present?
      @skus = @skus.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?

      sku_ids = @skus.present? ? @skus.pluck(:_id) : []
      allocated_skus = Allocation.where(:sku_id.in => sku_ids).not(allocated_to_id: nil)
      allocated_skus = allocated_skus.where(:content_process_id => params[:content_process_id]) if params[:content_process_id].present?
      allocated_sku_ids = allocated_skus.present? ? allocated_skus.pluck(:sku_id).uniq : []
      unallocated_sku_ids = sku_ids - allocated_sku_ids    
 
      render json: {status: true, allocated: allocated_sku_ids.count, unallocated: unallocated_sku_ids.count, columns: project_file.actual_headers.uniq}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET /skus/1
  def show
    render json: @sku
  end

  # POST /skus
  def create
    @sku = Sku.new(sku_params)

    if @sku.save
      render json: @sku, status: :created, location: @sku
    else
      render json: @sku.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /skus/1
  def update
    if @sku.update(sku_params)
      render json: @sku
    else
      render json: @sku.errors, status: :unprocessable_entity
    end
  end

  # DELETE /skus/1
  def destroy
    @sku.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sku
      @sku = Sku.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def sku_params
      params.require(:sku).permit! # (:project_file_id, :created_at, :updated_at)
    end
end

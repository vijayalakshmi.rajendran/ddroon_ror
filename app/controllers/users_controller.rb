class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
 # skip_before_action :authorize_request, only: [:index]

  # GET /users
  def index
    #UploadFileDataWorker.perform_async("Success")
    @users = params[:purpose] == "Allocation" || @current_user.role.role_name == "ADMIN" ? User.where(role_id: Role.find_by(role_name: "CONTENT PRODUCER").id) : @current_user.role.role_name == "QA ADMIN" ? 
User.where(role_id: Role.find_by(role_name: "QA USER").id) : User.not(role_id: Role.find_by(role_name: "SUPERADMIN").id)
    render json: @users.order(usename: :asc)
  end

  # GET /users/1
  def show
    render json: @user
  end

  # POST /users
  def create
    @user = User.new(user_params)

    @user.password = params[:password]
    @user.password_confirmation = params[:password_confirmation]

    @user.role_id = Role.find_by(level: 3).id if @current_user.role.role_name == ENV['ADMIN']
    @user.created_by_id = @current_user.id

    if @user.save
      NotificationMailer.welcome_mail(@user.id, params[:password]).deliver!
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  def destroy
    if @user.destroy
      render json: {status: true, message: "Account Deleted Successfully"}
    else
      render json: {status: false, message: "Something went wrong. Please try again"}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :username, :email_id, :password, :password_confirmation, :status, :role_id, :avatar, :designation, :date_of_joining, :fcm_token, :created_by_id)
    end
end

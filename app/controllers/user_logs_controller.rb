class UserLogsController < ApplicationController
  before_action :set_user_log, only: [:show, :update, :destroy]

  # GET /user_logs
  def index    
    if params[:user_id].present?        
        @user_logs = UserLog.where(user_id: params[:user_id])
    else
        @roles = Role.where({'level' => {'$gt' => @current_user.role.level}})
        role_ids = @roles.present? ? @roles.pluck(:id) : []
        user_ids = role_ids.present? ? User.in(role_id: role_ids).pluck(:id) : []
        @user_logs = UserLog.in(user_id: user_ids)
    end    
    start_date = params[:start_date].to_date if params[:start_date].present?
    end_date = params[:end_date].to_date if params[:end_date].present?
    @user_logs = @user_logs.where(log_date: start_date..end_date) if start_date.present? && end_date.present?  

    render json: @user_logs.paginate(page: params[:page], per_page: params[:per_page])
  end


 # GET - /user_log_details
  def user_log_details
    if params[:user_id].present?
        @user_logs = UserLog.where(user_id: params[:user_id])
    else
        @roles = Role.where({'level' => {'$gt' => @current_user.role.level}})
        role_ids = @roles.present? ? @roles.pluck(:id) : []
        user_ids = role_ids.present? ? User.in(role_id: role_ids).pluck(:id) : []
        @user_logs = UserLog.in(user_id: user_ids)
    end
    start_date = params[:start_date].to_date if params[:start_date].present?
    end_date = params[:end_date].to_date if params[:end_date].present?
    @user_logs = @user_logs.where(log_date: start_date..end_date) if start_date.present? && end_date.present?
    render json: {status: true, total_data: @user_logs.count}
  end


  # GET /user_logs/1
  def show
    render json: @user_log
  end


  # GET /log_checkout_time
  def log_checkout_time
    @user_log = UserLog.find_by(user_id: @current_user.id, log_date: Date.today) rescue nil
    begin 
      if @user_log.present?
	  logout_at_arr = @user_log.log_out_at
	  logout_at_arr.present? ? logout_at_arr << DateTime.now : logout_at_arr = [DateTime.now]
          @user_log.update(log_out_at: logout_at_arr)
          render json: {status: true, message: "Logout Time updated successfully"}
      else
          @user_log = UserLog.create(user_id: @current_user.id, log_date: Date.today, log_out_at: [DateTime.now])
          render json: {status: true, message: "Logout Time updated successfully"}
      end
    rescue => error
      render json: {status: false, message: error}
    end
  end


  # POST /user_logs
  def create
    @user_log = UserLog.new(user_log_params)

    if @user_log.save
      render json: @user_log, status: :created
    else
      render json: @user_log.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /user_logs/1
  def update
    if @user_log.update(user_log_params)
      render json: @user_log
    else
      render json: @user_log.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user_logs/1
  def destroy
    @user_log.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_log
      @user_log = UserLog.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_log_params
      params.require(:user_log).permit(:user_id, :log_date, :login_at, :log_out_at, :production_count, :production_info, :created_at, :updated_at)
    end
end

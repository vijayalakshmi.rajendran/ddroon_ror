class FiltersController < ApplicationController

  # GET - /get_sku_column_values
  def get_sku_column_values
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      @skus = Sku.where(project_file_id: params[:file_id])
      column_values = @skus.present? ? @skus.pluck(params[:column_name]).uniq : []
      render json: {status: true, data: column_values}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

   # GET - /get_taxonomy_column_values
  def get_taxonomy_column_values
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      @skus = Taxonomy.where(project_file_id: params[:file_id])
      column_values = @skus.present? ? @skus.pluck(params[:column_name]).uniq : []
      render json: {status: true, data: column_values}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

   # GET - /get_schema_column_values
  def get_schema_column_values
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      @skus = Schema.where(project_file_id: params[:file_id])
      column_values = @skus.present? ? @skus.pluck(params[:column_name]).uniq : []
      render json: {status: true, data: column_values}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET - /allocated_users
  def allocated_users
    @user_worksheet_logs = UserWorksheetLog.where(project_id: params[:project_id], project_file_id: params[:project_file_id], content_process_id: params[:content_process_id])
    user_ids = @user_worksheet_logs.pluck(:user_id) if @user_worksheet_logs.present?
    @users = User.in(id: user_ids)
    render json: @users
  end

  # GET - /log_users
  def log_users
    @roles = Role.where({'level' => {'$gt' => @current_user.role.level}})
    role_ids = @roles.present? ? @roles.pluck(:id) : []
    @users = User.in(role_id: role_ids) if role_ids.present?

    render json: @users
  end

end

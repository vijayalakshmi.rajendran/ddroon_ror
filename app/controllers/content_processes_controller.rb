class ContentProcessesController < ApplicationController
  before_action :set_content_process, only: [:show, :update, :destroy]

  # GET /content_processes
  def index
    @content_processes = ContentProcess.all

    render json: @content_processes
  end

  # GET /content_processes/1
  def show
    render json: @content_process
  end

  # POST /content_processes
  def create
    @content_process = ContentProcess.new(content_process_params)

    if @content_process.save
      render json: @content_process, status: :created
    else
      render json: @content_process.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /content_processes/1
  def update
    if @content_process.update(content_process_params)
      render json: @content_process
    else
      render json: @content_process.errors, status: :unprocessable_entity
    end
  end

  # DELETE /content_processes/1
  def destroy
    @content_process.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_content_process
      @content_process = ContentProcess.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def content_process_params
      params.require(:content_process).permit! # (:name, :process_columns, :collection_, :status_values, :created_by_id)
    end
end

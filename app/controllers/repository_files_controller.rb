require 'aws-sdk'
class RepositoryFilesController < ApplicationController
  before_action :set_repository_file, only: [:show, :update, :destroy]

  # GET /repository_files
  def index
    filter_query = {}
    request.query_parameters.keys.map{|i| filter_query[i] = params[i] if RepositoryFile.attribute_names.include?(i)}
    @repository_files = filter_query != {} ? RepositoryFile.where(filter_query) : RepositoryFile.all
    @repository_files = @repository_files.full_text_search(params[:search_key]) if params[:search_key].present? && params[:search_key].length > 0
    @repository_files = @repository_files.order("#{params[:sort_key]} #{params[:sort_order]}") if (params[:sort_key].present? && RepositoryFile.attribute_names.include?(params[:sort_key])) && params[:sort_order].present?
    render json: @repository_files.paginate(page: params[:page], per_page: params[:per_page])
  end

  # GET /repository_files/1
  def show
    render json: @repository_file
  end

  # GET /repository_projects
  def repository_projects
    existing_projects = Project.all.pluck(:name)
    other_projects = RepositoryFile.all.pluck(:project_name).uniq
    @repository_projects = existing_projects + other_projects
    render json: {status: true, data: @repository_projects.uniq}
  end

  # POST /repository_files
  def create
    begin
      file = params[:document]
      @repository_file = RepositoryFile.find_by(project_name: params[:project_name], name: file.original_filename) rescue nil

      if file.content_type == "application/pdf"
        file_headers = []
	      upload_status = false
      else
        sheet = Roo::Spreadsheet.open(params[:document])
        sheet_data = sheet.parse(headers: true)
        headers = sheet_data.shift
        file_headers = headers.keys
	      upload_status = true
      end

      unless @repository_file.present?
      	image = S3Store.new(file).store     
      	url = image.url if image.present?
      	@repository_file = RepositoryFile.new(name: file.original_filename, file_type: file.content_type, file_size: file.size, file_category: params[:file_category], s3_url: url, project_id: params[:project_id], project_name: params[:project_name], imported_by_id: @current_user.id, file_headers: file_headers, upload_in_progress: upload_status)
        UploadRepositoryFileWorker.perform_async(@repository_file.id, sheet_data, @current_user.email_id) if @repository_file.present? && file.content_type != "application/pdf"
      	if @repository_file.save
      	   render json: @repository_file, status: :created
      	else
      	   render json: @repository_file.errors, status: :unprocessable_entity
      	end
      else
      	render json: {status: false, message: "File Name already exists"}
      end
    rescue Exception => e
      render json: {status: false, message: "Something went wrong!", error: e}
    end   
  end
  
  
  # GET /move_schema_to_repository
  def move_schema_to_repository
    SchemaMigrationWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
    render json: {status: true, message: "Migration is in progress. You will be notified about the status via E-mail shortly."}
  end

  # GET /move_taxonomy_to_repository
  def move_taxonomy_to_repository
    TaxonomyMigrationWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
    render json: {status: true, message: "Migration is in progress. You will be notified about the status via E-mail shortly."}
  end

  # PATCH/PUT /repository_files/1
  def update
    if @repository_file.update(repository_file_params)
      render json: @repository_file
    else
      render json: @repository_file.errors, status: :unprocessable_entity
    end
  end

  # DELETE /repository_files/1
  def destroy
    if @repository_file.present?
      @s3 = Aws::S3::Resource.new
      @bucket = @s3.bucket("ddroon")
      @bucket.object(@repository_file.name).delete if @repository_file.name.present?
      if @repository_file.destroy
          render json: {status: true, message: "File deleted successfully"}
      else
          render json: {status: false, message: "Something went wrong"}
      end
    else
      render json: {status: false, message: "Invalid File Id"}
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_repository_file
      @repository_file = RepositoryFile.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def repository_file_params
      params.require(:repository_file).permit! #(:name, :s3_url, :created_at, :updated_at, :deleted_at)
    end
end

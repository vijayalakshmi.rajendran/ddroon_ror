class ClientTypesController < ApplicationController
  before_action :set_client_type, only: [:show, :update, :destroy]

  # GET /client_types
  def index
    @client_types = ClientType.all

    render json: @client_types
  end

  # GET /client_types/1
  def show
    render json: @client_type
  end

  # POST /client_types
  def create
    @client_type = ClientType.new(client_type_params)

    if @client_type.save
      render json: @client_type, status: :created
    else
      render json: @client_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /client_types/1
  def update
    if @client_type.update(client_type_params)
      render json: @client_type
    else
      render json: @client_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /client_types/1
  def destroy
    if @client_type.destroy
       render json: {status: true, message: "Deleted Successfully"}
    else
      render json: {status: false, message: "Something went wrong. Please try again"}
    end 
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_client_type
      @client_type = ClientType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def client_type_params
      params.require(:client_type).permit(:type_name, :created_by_id)
    end
end

require 'open-uri'
require 'nokogiri'
require 'parallel'

class SkuWorksheetsController < ApplicationController
    
	include WorksheetLog # To include the worksheet_log concern
	include UserProductionLog
 
	def send_sku_data

		@project = Project.find_by(db_name: params[:db_name])

		if @project.present?
			@project.tenancy!
			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?
			project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?

			@user_worksheet_log = UserWorksheetLog.find_by(sheet_id: params[:sheet_id]) rescue nil

			if @user_worksheet_log.present?
				render json: {status: false, message: "Sheet ID already have a unsaved data"}
			else
				# Authenticate a session with your Service Account
				session = GoogleDrive::Session.from_service_account_key("client_secret.json")
				# spreadsheet = session.spreadsheet_by_key("1JVK36aH0k_6_l04xmqKtC4l6yHlmgPJ64LS2-L9Z670")
				spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
				# Get the first worksheet
				worksheet = spreadsheet.worksheets.first
			
				sheet_headers = ["id", "#{params[:content_process_name]}_status"]
				project_file.actual_headers.delete("#{params[:content_process_name]}_status") 
				sheet_headers = sheet_headers + project_file.actual_headers
				
				# For clearing the sheet values
				(1..worksheet.num_rows).each do |row|
				    (1..worksheet.num_cols).each do |col|
					worksheet[row, col] = ""
				    end
				end
				worksheet.insert_rows(1, [sheet_headers])
				worksheet.save

				sku_ids = Allocation.where(:content_process_id => content_process.id, :allocated_to_id => params[:content_producer_id]).pluck(:sku_id) if content_process.present?
				@skus = Sku.where(project_file_id: project_file.id, :id.in => sku_ids)				

				sheet_data = []
				@skus.map do |sku|
					sku_data = sheet_headers.map do |i|
	                   if i == "#{params[:content_process_name]}_status"
					      allocation_data = sku.allocations.find_by(content_process_id: content_process.id) rescue nil					     
					      allocation_data.status if allocation_data.present?
					   else
					      sku[i]
					   end
					end
					sheet_data << sku_data
				end


				sheet_data.each_slice(1000) do |data|
					worksheet.insert_rows(worksheet.rows.count+1, data)
					worksheet.save
				end

				# worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
				# worksheet.save

				header_count =  project_file.input_headers.present? ? project_file.input_headers.count : 0

				# To hide the id field column in the sheet for end_user
				first_worksheet = spreadsheet.worksheets.first
				sheet_id = first_worksheet.sheet_id

				first_worksheet.add_request(
				  {
				    add_protected_range: {
				      protected_range: {
				        range: {
				          sheet_id: sheet_id,
			          start_column_index: 0,
				          end_column_index: 0,
				        },
				        description: 'Protecting total row',
				        warning_only: true,
				        requesting_user_can_edit: false,
				      }
				    }
				  }
				)

				first_worksheet.save

				first_worksheet.add_request(
				  {
				    add_protected_range: {
				      protected_range: {
				        range: {
				          sheet_id: sheet_id,
				          start_column_index: 2,
				          end_column_index: header_count+2,
				        },
				        description: 'Protecting total row',
				        warning_only: true,
				        requesting_user_can_edit: false,
				      }
				    }
				  }
				)

				first_worksheet.save

				# To protect the id field column cells from editing by mistake
				first_worksheet.add_request(
					{
					  	update_dimension_properties: {
						    range: {
						      sheet_id: sheet_id,
						      dimension: 'COLUMNS',
						      start_index: 0,
						      end_index: 1,
						    },
						    properties: {
						      hidden_by_user: true,
						    },
						    fields: 'hidden_by_user',
						}
					}
				)

				first_worksheet.save

				# To save the content producer worksheet activity
				create_worksheet_log(@project.id, project_file.id, content_process.id, @current_user.id, params[:sheet_id])

				render json: {status: true, message: "Data Sent Successfully"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end
	

	def save_sku_data
		@project = Project.find_by(db_name: params[:db_name])
		registration_ids = []
		production_ids = []
		if @project.present?
			
			@project.tenancy!

			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])			
			worksheet = spreadsheet.worksheets[1]
			if worksheet.present? 
				sheet_data = worksheet.rows
				column_names = sheet_data[0]

				project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?
				content_process = ContentProcess.find_by(name: params[:content_process_name]) if params[:content_process_name].present?
				
				new_header_values = column_names - ["id"] if column_names.present?
	            project_file.update(actual_headers: new_header_values.uniq) if new_header_values.present?

	            ind = column_names.index("id")
	            sku_ids = sheet_data[1..-1].collect {|data| data[ind]}
	            @skus = Sku.where(:id.in => sku_ids) if sku_ids.present?
	            @allocations = Allocation.where(content_process_id: content_process.id, :sku_id.in => sku_ids) if content_process.present? && sku_ids.present?

				#sheet_data[1..-1].each_slice(1000) do |slice_data|
				#	Parallel.map(slice_data, in_threads: 4) do |data|				
				#	Parallel.map(sheet_data[1..-1], in_threads: 4) do |data|
					sheet_data[1..-1].map do |data|
						@project.tenancy!
						updatable_data = {}
						column_names.each_with_index do |col_name, index|					
							unless project_file.input_headers.include? col_name
								updatable_data[col_name] = data[index]
							end
						end

						@sku = @skus.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
						if @sku.present?
						    @sku.update(updatable_data)
							@allocation = @allocations.find_by(sku_id: @sku.id) if @sku.present?
							status = data[column_names.index("#{params[:content_process_name]}_status")]
							alloc_data = status.downcase == "reallocate" ? {status: status, allocated_to_id: nil} : {status: status}					
							if @allocation.present?
								production_ids << @allocation.sku_id if @allocation.status != status && status.downcase != "reallocate" && status != ""
						                if status.downcase == "reallocate" && @allocation.allocated_by.fcm_token.present?
					        	                registration_ids << @allocation.allocated_by.fcm_token unless registration_ids.include?(@allocation.allocated_by.fcm_token)
			                			end
								status.downcase == "reallocate" ? @allocation.delete : @allocation.update(alloc_data)
							end					
						end						
					end
				#end				

				if registration_ids != []
					fcm = FCM.new(ENV["FCM_SERVER_KEY"])
				    options = { "notification": {"title": "DDROON", "body": "Hi, #{@current_user.username} have reallocated some of the SKUs in the file #{project_file.file_name} of the project #{@project.name} for the process #{params[:content_process_name]}"}}
				    fcm.send(registration_ids, options)
				end

				# For clearing the sheet values
				(1..worksheet.num_rows).each do |row|
				    (1..worksheet.num_cols).each do |col|
							worksheet[row, col] = ""
				    end
				end
				worksheet.save

				# To log the production count of a user
				update_production_info(@current_user.id, @project.name, production_ids, params[:content_process_name])
				# To delete the content producer worksheet activity when the activity gets completed
				delete_worksheet_log(@project.id, project_file.id, content_process.id, @current_user.id, params[:sheet_id])	

				render json: {status: true, message: "Data saved Successfully"}
			else
				render json: {status: false, message: "No worksheet found to save"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end		
	end



end







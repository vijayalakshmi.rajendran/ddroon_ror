require 'open-uri'
require 'nokogiri'
require 'parallel'
require 'csv'

class DownloadSkuWorksheetsController < ApplicationController   
 
 	# GET - /download_sku_worksheets
	def download_sku_data
		@project = Project.find_by(db_name: params[:db_name])
		if @project.present?
			@project.tenancy!
			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?
			project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?
						
			sheet_headers = ["id", "#{params[:content_process_name]}_status"]
			project_file.actual_headers.delete("#{params[:content_process_name]}_status") 
			sheet_headers = sheet_headers + project_file.actual_headers				

			sku_ids = Allocation.where(:content_process_id => content_process.id, :allocated_to_id => params[:content_producer_id]).pluck(:sku_id) if content_process.present?
			@skus = Sku.where(project_file_id: project_file.id, :id.in => sku_ids)				

			result = Axlsx::Package.new do |obj|
                obj.workbook.add_worksheet(name: "Sheet1") do |sheet|
	                sheet.add_row sheet_headers	        
	        		@skus.map do |sku|
						sku_data = sheet_headers.map do |i|
			                if i == "#{params[:content_process_name]}_status"
					            allocation_data = sku.allocations.find_by(content_process_id: content_process.id) rescue nil					     
						        allocation_data.status if allocation_data.present?
						    else
						        sku[i]
						    end
						end
						sheet.add_row sku_data
					end                
                end
            end
            @file_path = "#{Rails.root}/public/#{project_file.file_name}"
            result.serialize(@file_path)
            begin 
            	send_file @file_path, :type => project_file.file_type, :disposition => 'attachment'
	#	render json: {status: true, message: "File downloaded"}
            rescue => error
            	render json: {status: false, message: error.message}
            ensure
                 logger.info("---------------------------------success---------------------------------------------------------------")
            	#File.delete(@file_path) if File.exist?(@file_path)
            end			
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end			


end







class AllocationsController < ApplicationController
  before_action :set_allocation, only: [:show, :update, :destroy]

  include SkuAllocation # To include the 'sku_allocation' concern

  # GET /allocations
  def index
    @response = allocation_summary(params[:db_name], params[:file_id], params[:column_name], params[:content_process_id])
    render json: @response
  end

  # GET /allocations/1
  def show
    render json: @allocation
  end

  # POST /allocations
  def create
    @response = allocate_skus(params[:db_name], params[:content_process_id], params[:file_id], params[:content_producer_id], params[:admin_id], params[:ids])
    render json: @response
  end

  # POST /multiple_allocation
  def multiple_allocation
     @response = allocate_skus_to_multiple_users(params[:db_name], params[:content_process_id], params[:file_id], params[:admin_id], params[:allocation_data])
    render json: @response
  end

  # PATCH/PUT /allocations/1
  def update
    if @allocation.update(allocation_params)
      render json: @allocation
    else
      render json: @allocation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /allocations/1
  def destroy
    @allocation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_allocation
      @allocation = Allocation.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def allocation_params
      params.require(:allocation).permit(:content_process_id, :allocated_to, :allocated_by, :status, :created_at, :updated_at)
    end
end

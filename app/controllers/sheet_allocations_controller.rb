class SheetAllocationsController < ApplicationController

	def send_data_for_allocation

		@project = Project.find_by(db_name: params[:db_name]) rescue nil

		if @project.present?
			
			@project.tenancy!

			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")

			# spreadsheet = session.spreadsheet_by_key("1JVK36aH0k_6_l04xmqKtC4l6yHlmgPJ64LS2-L9Z670")
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			# Get the first worksheet

			worksheet = spreadsheet.worksheets.first

			# sheet_headers = Sku.attribute_names - ["created_at", "updated_at", "project_file_id"]
			# sheet_headers << "allocated_to"

			content_process = ContentProcess.find(params[:content_process_id]) rescue nil

			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
			#sheet_headers = ["id"] + project_file.actual_headers + ["allocated_to"]
			sheet_headers = ["id"] + project_file.actual_headers + ["#{content_process.name}_by"]

			# Dumps all cells.
			(1..worksheet.num_rows).each do |row|
			  (1..worksheet.num_cols).each do |col|
			    worksheet[row, col] = ""
			  end
			end

			worksheet.insert_rows(1, [sheet_headers])
			worksheet.save

			@skus = Sku.where(project_file_id: params[:file_id])
			# @skus = Sku.where(project_file_id: params[:file_id]).not_in('allocations.content_process_id' => params[:content_process_id])

			sheet_data = []
			@skus.map do |sku|
				allocation_data = sku.allocations.find_by(:content_process_id => params[:content_process_id]) rescue nil if sku.allocations.present? && params[:content_process_id].present?
				# unless allocation_data.present?
				sku_data = sheet_headers.map do |i| 
				  if i == "#{content_process.name}_by"
				     allocation_data.present? && allocation_data.allocated_to.present? ? allocation_data.allocated_to.username : nil
				  else
				     sku[i]
				  end
				end
				# sku_data << allocation_data.allocated_to.username if allocation_data.present? && allocation_data.allocated_to.present? 
				sheet_data << sku_data
				# end
			end

			worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
			worksheet.save

			# To hide the id field column in the sheet for end_user
			first_worksheet = spreadsheet.worksheets.first
			sheet_id = first_worksheet.sheet_id

			first_worksheet.add_request(
			  {
			    add_protected_range: {
			      protected_range: {
			        range: {
			          sheet_id: sheet_id,
			          # start_row_index: 0,
			          # end_row_index: 1,
			          start_column_index: 0,
			          end_column_index: 1,
			        },
			        description: 'Protecting total row',
			        warning_only: true,
			        requesting_user_can_edit: false,
			      }
			    }
			  }
			)

			first_worksheet.save

			# To protect the id field column cells from editing by mistake
			first_worksheet.add_request(
				{
				  	update_dimension_properties: {
					    range: {
					      sheet_id: sheet_id,
					      dimension: 'COLUMNS',
					      start_index: 0,
					      end_index: 1,
					    },
					    properties: {
					      hidden_by_user: true,
					    },
					    fields: 'hidden_by_user',
					}
				}
			)

			first_worksheet.save


			render json: {status: true, message: "Data Sent Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end
	

	def save_allocation_data

		@project = Project.find_by(db_name: params[:db_name]) rescue nil

		if @project.present?
			
			@project.tenancy!

			registration_ids = []
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])

			worksheet = spreadsheet.worksheets.first

			sheet_data = worksheet.rows

			column_names = sheet_data[0]

			content_process = ContentProcess.find(params[:content_process_id]) if params[:content_process_id].present?
			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
			admin = User.find(params[:admin_id]) if params[:admin_id].present?

			# sku_data = []
			sheet_data[1..-1].map do |data|
				updatable_data = {}
				# column_names.each_with_index do |col_name, index| 
				# 	if col_name == "allocated_to"
				# 		content_producer = User.find_by(username: data[index]) if data[index].present? && data[index] != ""
				# 	  	updatable_data["allocated_to_id"] = content_producer.id if content_producer.present?
				# 	else
				# 	 	updatable_data[col_name] = data[index]		
				# 	end			
				# end

				sku = Sku.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
				updatable_data["sku_id"] = sku.id if sku.present?

				content_producer = User.find_by(username: data[column_names.index("#{content_process.name}_by")]) if column_names.index("#{content_process.name}_by").present? && data[column_names.index("#{content_process.name}_by")].present?		
				
				updatable_data["allocated_to_id"] = content_producer.id if content_producer.present?
				updatable_data["allocated_by_id"] = admin.id if admin.present?
				updatable_data["project_file_id"] = project_file.id if project_file.present?
				updatable_data["content_process_id"] = content_process.id if content_process.present?

				@allocation = Allocation.find_by(sku_id: sku.id, content_process_id: content_process.id) rescue nil				

				if @allocation.present? 
					if content_producer.present? && content_producer.fcm_token.present?
                		registration_ids << content_producer.fcm_token unless registration_ids.include?(content_producer.fcm_token) && @allocation.allocated_to_id != content_producer.id
		        	end
					@allocation.update_attributes(updatable_data)
				else
					if content_producer.present? && content_producer.fcm_token.present?
	                	registration_ids << content_producer.fcm_token unless registration_ids.include?(content_producer.fcm_token)
			        end
					Allocation.create(updatable_data)
				end
										
			end		

			fcm = FCM.new(ENV["FCM_SERVER_KEY"])
		    options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
		    fcm.send(registration_ids, options)
			render json: {status: true, message: "Allocation Successfull"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
		
	end


	# def save_allocation_data

	# 	@project = Project.find_by(db_name: params[:db_name])

	# 	if @project.present?
			
	# 		@project.tenancy!

	# 		# Authenticate a session with your Service Account
	# 		session = GoogleDrive::Session.from_service_account_key("client_secret.json")
	# 		# Get the first worksheet
	# 		spreadsheet = session.spreadsheet_by_key(params[:sheet_id])

	# 		worksheet = spreadsheet.worksheets.first

	# 		sheet_data = worksheet.rows

	# 		column_names = sheet_data[0]

	# 		sku_data = []
	# 		sheet_data[1..-1].map do |data|
	# 			updatable_data = {}
	# 			column_names.each_with_index do |col_name, index| 
	# 				if col_name == "allocated_to"
	# 					content_producer = User.find_by(username: data[index]) if data[index].present? && data[index] != ""
	# 				  	updatable_data["allocated_to_id"] = content_producer.id if content_producer.present?
	# 				else
	# 				 	updatable_data[col_name] = data[index]		
	# 				end			
	# 			end
	# 			updatable_data["allocated_by_id"] = params[:admin_id]
	# 			updatable_data["project_file_id"] = params[:file_id]
	# 			updatable_data["content_process_id"] = params[:content_process_id]
	# 			sku_data << { update_one:
	# 			    {
	# 			      filter: { _id: data[0] },
	# 			      update: { :'$set' => updatable_data}
	# 			    }
	# 			}
	# 		end

	# 		Sku.collection.bulk_write(sku_data)		

	# 		render json: {status: true, message: "Allocation Successfull"}
	# 	else
	# 		render json: {status: false, message: "Invalid Project Code"}
	# 	end
		
	# end

end

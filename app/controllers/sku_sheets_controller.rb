class SkuSheetsController < ApplicationController

	# GET - /sku_sheets
	def send_data_to_view

		@project = Project.find_by(db_name: params[:db_name])

		if @project.present?
			
			@project.tenancy!

			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# spreadsheet = session.spreadsheet_by_key("1JVK36aH0k_6_l04xmqKtC4l6yHlmgPJ64LS2-L9Z670")
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			# Get the first worksheet
			worksheet = spreadsheet.worksheets.first

			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
			# sheet_headers = ["id"] + project_file.actual_headers
			sheet_headers = ["id"]
			allocation_headers = {}
			project_file.file_process_mappings.map do |fp_mapping|
	  		   process_allocated_to = "#{fp_mapping.content_process.name}_by" if fp_mapping.content_process.present?
			   # sheet_headers << process_allocated_to unless sheet_headers.include?(process_allocated_to)
			   sheet_headers << process_allocated_to
			   project_file.actual_headers.delete(process_allocated_to)
			   allocation_headers[process_allocated_to] = fp_mapping.content_process_id
			end

			sheet_headers = sheet_headers + project_file.actual_headers

			# Dumps all cells.
			(1..worksheet.num_rows).each do |row|
			  (1..worksheet.num_cols).each do |col|
			    worksheet[row, col] = ""
			  end
			end

                worksheet.add_request({
		          delete_dimension: {
		            range: {
		              sheet_id: worksheet.sheet_id,
		              dimension: "COLUMNS",
		              start_index: 1,
		              end_index: 26
		            }
		          }
		        })

			worksheet.insert_rows(1, [sheet_headers])
			worksheet.save

			@skus = Sku.where(project_file_id: params[:file_id])

			sheet_data = []
			@skus.map do |sku|				
				sku_data = []
				sheet_headers.map do |i| 
				   if allocation_headers.keys.include?(i)
				      process_allocation = sku.allocations.find_by(content_process_id: allocation_headers[i]) rescue nil if sku.allocations.present?
				      content_producer_name = process_allocation.present? && process_allocation.allocated_to.present? ? process_allocation.allocated_to.username : ""
				      sku_data << content_producer_name
				   else				   
				      sku_data <<  sku[i]
				   end
				end
				sheet_data << sku_data
			end

		#	worksheet.insert_rows(worksheet.rows.count, sheet_data.length)			

			worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
			worksheet.save

			render json: {status: true, message: "Data Sent Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end

	# POST - /sku_sheets
	def save_sku_data

		@project = Project.find_by(db_name: params[:db_name])

		if @project.present?
			
			@project.tenancy!

			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])

			worksheet = spreadsheet.worksheets.first

			sheet_data = worksheet.rows

			column_names = sheet_data[0]

			project_file = ProjectFile.find_by(file_name: params[:file_name]) if params[:file_name].present?
			content_process = ContentProcess.find_by(name: params[:content_process_name]) if params[:content_process_name].present?

			new_header_values = project_file.actual_headers - (column_names - ["id"])
			total_header_values = project_file.actual_headers + new_header_values
			project_file.update(actual_headers: total_header_values)

			sheet_data[1..-1].map do |data|
				updatable_data = {}
				column_names.each_with_index do |col_name, index| 
					# if col_name == "status"
					# 	status_key = content_process.name + "_status" if content_process.present?
					# 	default_value = content_process.status_values[:default] if content_process.present? && content_process.status_values.present?
					# 	status_value = data[index].present? && data[index].length > 0 ? data[index] : default_value
					# 	updatable_data[status_key] = status_value
					# else
					#unless project_file.input_headers.include?(col_name)
						updatable_data[col_name] = data[index]
					#end
					# end
				end

				@sku = Sku.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?

				if @sku.present?
					@sku.update(updatable_data)
					@allocation = Allocation.find_by(sku_id: @sku.id, content_process_id: content_process.id) rescue nil if content_process.present?
					# default_value = content_process.status_values[:default] if content_process.present? && content_process.status_values.present?
					status = data[column_names.index("#{params[:content_process_name]}_status")] if column_names.index("#{params[:content_process_name]}_status").present?
					@allocation.update(status: status) if @allocation.present?
				end

			end
			
			# To delete the content producer worksheet activity when the activity gets completed
			sheet_logs = UserWorksheetLog.where(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:content_producer_id]) rescue nil if params[:content_producer_id].present?
      		sheet_logs.delete_all if sheet_logs.present?

			render json: {status: true, message: "Data saved Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
    end

end

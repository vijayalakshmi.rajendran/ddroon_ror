class TaxonomiesController < ApplicationController
  before_action :set_taxonomy, only: [:show, :update, :destroy]

  # GET /taxonomies
  def index
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
        @project.tenancy!
         
        if params[:search_key].present? && params[:search_key].length > 0
	    search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
            @taxonomies = Taxonomy.full_text_search(params[:search_key], match: search_strategy)
            @taxonomies = @taxonomies.where(project_file_id: params[:file_id]) if @taxonomies.present?
        else
            @taxonomies = Taxonomy.where(project_file_id: params[:file_id])
        end
        @taxonomies = @taxonomies.order("#{params[:sort_key]} #{params[:sort_order]}") if params[:sort_key].present? && params[:sort_order].present?

        if params[:assigned].present?
            column_values = params[:column_values].split(".") if params[:column_values].present?
            taxonomy_ids = @taxonomies.pluck(:_id) 
            assigned_taxonomy_ids = Taxonomy.where(:allocated_to_id.ne => nil).pluck(:_id)
            unassigned_taxonomy_ids =  taxonomy_ids - assigned_taxonomy_ids
            @taxonomies = params[:assigned] == "true" || params[:assigned] == true ? @taxonomies.in(params[:column_name] => column_values, id: assigned_taxonomy_ids) : @taxonomies.in(params[:column_name] => column_values, id: unassigned_taxonomy_ids)
            @taxonomies = @taxonomies.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?
            @taxonomies = @taxonomies.paginate(page: params[:page], per_page: params[:per_page]) 
	          taxonomy_data = []
            @taxonomies.map do |taxonomy|
            	taxonomy_data << {sku: taxonomy, allocated_to: taxonomy.allocated_to, allocated_by: taxonomy.allocated_by}
            end
            render json: taxonomy_data
        else
            render json: @taxonomies.paginate(page: params[:page], per_page: params[:per_page])
        end
    else
        render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET /taxonomies_pivot
  def pivot_view
    project = Project.find_by(db_name: params[:db_name])
    if project.present?  
      project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
      @taxonomies = Taxonomy.where(project_file_id: project_file._id)
      @pivot_data = {}
      column_names = params[:column_names].present? ? params[:column_names].split(".") : nil
      if column_names.present?
        column_names.map{|i| @pivot_data[i] = @taxonomies.pluck(i).uniq}
      else
        project_file.actual_headers.map{|i| @pivot_data[i] = @taxonomies.pluck(i).uniq}
      end
      render json: @pivot_data
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

 
  # GET /taxonomy_details
  def taxonomy_details
    project = Project.find_by(db_name: params[:db_name])
    if project.present?
        project.tenancy!
        project_file = ProjectFile.find(params[:file_id]) rescue nil
        
        if params[:search_key].present? && params[:search_key].length > 0
            search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
            @taxonomies = Taxonomy.full_text_search(params[:search_key], match: search_strategy)
            @taxonomies = @taxonomies.where(project_file_id: project_file._id) if project_file.present? && @taxonomies.present?
        else
            @taxonomies = Taxonomy.where(project_file_id: project_file._id) if project_file.present?
        end
        column_values = params[:column_values].split(".") if params[:column_values].present?
        @taxonomies = params[:column_name].present? && column_values.present? ? @taxonomies.in(params[:column_name] => column_values) : @taxonomies
        @taxonomies = @taxonomies.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?
        taxonomy_ids = @taxonomies.pluck(:_id) if @taxonomies.present?
        allocated_count = @taxonomies.where(:allocated_to_id.ne => nil).count
        unallocated_count = @taxonomies.where(allocated_to_id: nil).count
        render json: {status: true, total_count: @taxonomies.count, allocated: allocated_count, unallocated: unallocated_count, columns: project_file.actual_headers.uniq}
    else
        render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # GET /taxonomy_files
  def taxonomy_files
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      content_process = ContentProcess.find_by(name: "Taxonomy") rescue nil
      file_process_mappings = FileProcessMapping.where(content_process_id: content_process.id)
      file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
      @project_files = file_ids.present? ? ProjectFile.where(:id.in => file_ids) : []
      render json: @project_files
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end



  # GET /taxonomies/1
  def show
    render json: @taxonomy
  end

  # POST /taxonomies
  def create
    @taxonomy = Taxonomy.new(taxonomy_params)

    if @taxonomy.save
      render json: @taxonomy, status: :created, location: @taxonomy
    else
      render json: @taxonomy.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /taxonomies/1
  def update
    if @taxonomy.update(taxonomy_params)
      render json: @taxonomy
    else
      render json: @taxonomy.errors, status: :unprocessable_entity
    end
  end

  # DELETE /taxonomies/1
  def destroy
    @taxonomy.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_taxonomy
      @taxonomy = Taxonomy.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def taxonomy_params
      params.require(:taxonomy).permit! # (:project_file_id, :allocated_to_id, :allocated_by_id, :status, :created_at, :updated_at)
    end
end






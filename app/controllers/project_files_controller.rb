require 'roo'

class ProjectFilesController < ApplicationController
  before_action :set_project_file, only: [:show, :update, :destroy]
  

  # GET /project_files
  def index
    @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?  
      @project.tenancy!
        file_process_mappings = FileProcessMapping.where(content_process_id: params[:content_process_id])
        file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
        if @current_user.is_QA_admin?
          @project_files = file_ids.present? ? ProjectFile.where(:id.in => file_ids, :audit_stage => "inAudit") : []
        else
          @project_files = file_ids.present? ? ProjectFile.where(:id.in => file_ids) : []
        end
      render json: @project_files
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  
  # GET /trashed_files
  def trashed_files
     @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?
      @project.tenancy!
      file_process_mappings = FileProcessMapping.deleted.where(content_process_id: params[:content_process_id])
      file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
      @project_files = file_ids.present? ? ProjectFile.deleted.where(:id.in => file_ids) : []
      render json: @project_files
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # GET /project_files/1
  def show
    render json: @project_file
  end


  # POST /project_files
  def create
    @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?
      @project.tenancy!
      if params[:imported_file].present?
        @same_file = ProjectFile.find_by(file_name: params[:imported_file].original_filename) rescue nil
        unless @same_file.present?
            sheet = Roo::Spreadsheet.open(params[:imported_file])
            sheet_data = sheet.parse(headers: true)
            headers = sheet_data.shift
            file_processes = JSON.parse(params[:file_process_mappings_attributes])
          begin
            if file_processes.count == 1 
              content_process = ContentProcess.find(file_processes[0]["content_process_id"]) rescue nil
              if content_process.present? && content_process.name == "Taxonomy"
                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, upload_in_progress: true, file_process_mappings_attributes: file_processes})
                data_for_the_process = "Taxonomy"
              elsif content_process.present? && content_process.name == "Schema"
                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, upload_in_progress: true, file_process_mappings_attributes: file_processes})
                data_for_the_process = "Schema"
              else
                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, upload_in_progress: true, file_process_mappings_attributes: file_processes})
                data_for_the_process = "Sku"
              end
            else 
              @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, upload_in_progress: true, file_process_mappings_attributes: file_processes})
              data_for_the_process = "Sku"
            end

            if @project_file.save
              UploadFileWorker.perform_async(@project.id, @project_file.id, sheet_data, data_for_the_process, @current_user.id)		    
              render json: @project_file, status: :created
            else
              render json: @project_file.errors, status: :unprocessable_entity
            end
	        rescue => e
            render json: {status: false, message: e}
          end
        else
          render json: {status: false, message: "File Name already exists"}
        end
      else
        render json: {status: false, message: "Please import a file"}
      end
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # PATCH/PUT /project_files/1
  def update
    if @project_file.update(project_file_params)
      render json: @project_file
    else
      render json: @project_file.errors, status: :unprocessable_entity
    end
  end


  # POST /update_input_headers
  def update_input_headers
    @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?  
      @project.tenancy!
      @project_file = ProjectFile.find(params[:file_id]) rescue nil
      if @project_file.update(input_headers: params[:input_headers])
        render json: {status: true, message: "Input headers updated successfully"}
      else
        render json: {status: false, message: "Invalid File ID"}
      end
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end  


  # POST /archive_file
  def archive_file
    @project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?  
      @project.tenancy!
      @project_file = ProjectFile.find(params[:file_id]) rescue nil
      if @project_file.update(is_latest_version: params[:is_latest_version])
        render json: {status: true, message: "File Archived successfully"}
      else
        render json: {status: false, message: "Invalid File ID"}
      end
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # DELETE /project_files/1
  def destroy
	@project_file.allocations.delete_all
    if @project_file.destroy
       render json: {status: true, message: "File deleted successfully"}
    else
       render json: {status: false, message: "Something went wrong. Try Again"}
    end
  end


   # GET /restore_file/1
  def restore_file
     @project = Project.find(params[:project_id]) rescue nil
     if @project.present?
	@project.tenancy!
	@project_file = ProjectFile.deleted.find(params[:id]) rescue nil
	if @project_file.present? && @project_file.restore(:recursive => true)
	   render json: {status: true, message: "File restored successfully"}
	else
	   render json: {status: false, message: "Something went wrong. Try again!"}
	end
     end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project_file
      @project_file = ProjectFile.find(params[:id])
    end

    # # Only allow a trusted parameter "white list" through.
    # def project_file_params
    #   params.require(:project_file).permit(:file_name, :file_type, :file_size, :sku_count, :deadline, :imported_by_id, :actual_headers, :mapped_headers, {:content_process_ids => []})
    # end

    # Only allow a trusted parameter "white list" through.
    def project_file_params
      params.require(:project_file).permit(:file_name, :file_type, :file_size, :sku_count, :deadline, :imported_by_id, :actual_headers, :mapped_headers, :is_latest_version, file_process_mappings_attributes: [:id, :content_process_id, :deadline, :project_file_id, :type_of_file])
    end
end





# -----------------------------------------------------------------------------------------------------------------



 # # POST /project_files
 #  def create
 #    @project = Project.find_by(db_name: params[:db_name]) rescue nil
 #    if @project.present?
 #      @project.tenancy!
 #      if params[:imported_file].present?
 #        @same_file = ProjectFile.find_by(file_name: params[:imported_file].original_filename) rescue nil
 #        unless @same_file.present?
 #            sheet = Roo::Spreadsheet.open(params[:imported_file])
 #            sheet_data = sheet.parse(headers: true)
 #            headers = sheet_data.shift
 #            file_processes = JSON.parse(params[:file_process_mappings_attributes])
 #          begin
 #            if file_processes.count == 1 
 #              content_process = ContentProcess.find(file_processes[0]["content_process_id"]) rescue nil
 #              if content_process.present? && content_process.name == "Taxonomy"
 #                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, taxonomies_attributes: sheet_data, file_process_mappings_attributes: file_processes})
 #              elsif content_process.present? && content_process.name == "Schema"
 #                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, schemas_attributes: sheet_data, file_process_mappings_attributes: file_processes})
 #              else
 #                @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, skus_attributes: sheet_data, file_process_mappings_attributes: file_processes})
 #              end
 #            else 
 #              @project_file = ProjectFile.new({file_name: params[:imported_file].original_filename, file_type: params[:imported_file].content_type, file_size: params[:imported_file].size, sku_count: sheet_data.count, deadline: params[:deadline], imported_by_id: params[:imported_by_id], input_headers: headers.keys, actual_headers: headers.keys, mapped_headers: headers, skus_attributes: sheet_data, file_process_mappings_attributes: file_processes})
 #            end

 #            if @project_file.save
 #              render json: @project_file, status: :created
 #            else
 #              render json: @project_file.errors, status: :unprocessable_entity
 #            end
 #    rescue => e
 #            render json: {status: false, message: e}
 #          end
 #        else
 #          render json: {status: false, message: "File Name already exists"}
 #        end
 #      else
 #        render json: {status: false, message: "Please import a file"}
 #      end
 #    else
 #      render json: {status: false, message: "Invalid Project Code"}
 #    end
 #  end

require 'jwt'

class AuthenticationController < ApplicationController
  skip_before_action :authorize_request, only: [:login, :verify_username, :verify_code]

  # POST /auth/login
  def login
    @user = User.find_by(username: params[:username]) rescue nil
    unless @user.present?
	      @user = User.find_by(email_id: params[:username]) rescue nil
    end
    
    if @user.present? && @user&.authenticate(params[:password])
      time = Time.now + 24.hours.to_i
      token = JsonWebToken.encode(@user.id, time)
      
      user_data = {_id: @user._id, username: @user.username, email_id: @user.email_id, designation: @user.designation, date_of_joining: @user.date_of_joining, role: @user.role, fcm_token: @user.fcm_token }
      # To save the Login time in the User Log Model
    @user_log = UserLog.find_by(user_id: @user.id, log_date: Date.today) rescue nil
    begin
        if @user_log.present?
	  login_at_arr = @user_log.login_at
	  login_at_arr.present? ? login_at_arr << DateTime.now : login_at_arr = [DateTime.now]
          @user_log.update(login_at: login_at_arr)
        else
          @user_log = UserLog.create(user_id: @user.id, log_date: Date.today, login_at: [DateTime.now])
        end
	render json: { token: token, exp: time.strftime("%m-%d-%Y %H:%M"), user: user_data}, status: :ok
    rescue => error
        render json: {status: false, message: error}
    end
	
#      render json: { token: token, exp: time.strftime("%m-%d-%Y %H:%M"), user: user_data}, status: :ok
    else
	    render json: {status: false, message: "Username or Password is incorrect"}
     # render json: { error: 'Username or Password is incorrect' }, status: :unauthorized
    end
  end



  # GET - for identifying the user based on the username given
  def verify_username
    response = User.identify_user(params[:email_id])
    render json: response
  end


  # POST - For verifying the verification code entered by the user while changing password
  def verify_code
    response = User.verify_code(params[:id], params[:verification_code], params[:password], params[:password_confirmation])
    render json: response
  end


  # POST - For changing the password of the user when account is logged in
  def reset_password
    response = User.reset_password(params[:id], params[:old_password], params[:password], params[:password_confirmation])
    render json: response
  end


  private

  def login_params
	   params.permit(:username, :password)
  end

end




class RepositoryFileDatasController < ApplicationController

  # GET /repository_file_datas
  def index    
	search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
  	@repository_file_datas = RepositoryFileData.full_text_search(params[:search_key], match: search_strategy) if params[:search_key].present? && params[:search_key].length > 0
  	if params[:search_key].present? && params[:search_key].length > 0
    		@repository_file_datas = @repository_file_datas.where(repository_file_id: params[:repository_file_id]) if params[:repository_file_id].present?    
   	else
   		@repository_file_datas = RepositoryFileData.where(repository_file_id: params[:repository_file_id]) if params[:repository_file_id].present?
   	end
    @repository_file_datas = @repository_file_datas.order("#{params[:sort_key]} #{params[:sort_order]}") if (params[:sort_key].present? && RepositoryFileData.attribute_names.include?(params[:sort_key])) && params[:sort_order].present?
    render json: @repository_file_datas.paginate(page: params[:page], per_page: params[:per_page])
  end


    # GET /repository_file_details/1
  def repository_file_details
    @repository_file = RepositoryFile.find(params[:id]) rescue nil
    if params[:search_key].present? && params[:search_key].length > 0
        search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
        @repository_file_datas = RepositoryFileData.full_text_search(params[:search_key], match: search_strategy)
        @repository_file_datas = @repository_file_datas.where(repository_file_id: @repository_file.id)
    else
        @repository_file_datas = @repository_file.repository_file_datas
    end
    render json: {status: true, total_count: @repository_file_datas.count, columns: @repository_file.file_headers.uniq}
  end



end

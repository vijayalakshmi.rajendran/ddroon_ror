class SchemasController < ApplicationController
  before_action :set_schema, only: [:show, :update, :destroy]

  # GET /schemas
  def index
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
        @project.tenancy!     
        
        if params[:search_key].present? && params[:search_key].length > 0
	    search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
#byebug        
#	    search_strategy = "all"
	    @schemas = Schema.full_text_search(params[:search_key], match: search_strategy)
            @schemas = @schemas.where(project_file_id: params[:file_id]) if @schemas.present?
        else
            @schemas = Schema.where(project_file_id: params[:file_id])
        end

        @schemas = @schemas.order("#{params[:sort_key]} #{params[:sort_order]}") if params[:sort_key].present? && params[:sort_order].present?

        if params[:assigned].present?
            column_values = params[:column_values].split(".") if params[:column_values].present?
            schema_ids = @schemas.pluck(:_id) 
            assigned_schema_ids = Schema.where(:allocated_to_id.ne => nil).pluck(:_id)
            unassigned_schema_ids =  schema_ids - assigned_schema_ids
            @schemas = params[:assigned] == "true" || params[:assigned] == true ? @schemas.in(params[:column_name] => column_values, id: assigned_schema_ids) : @schemas.in(params[:column_name] => column_values, id: unassigned_schema_ids)
            @schemas = @schemas.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?
            @schemas = @schemas.paginate(page: params[:page], per_page: params[:per_page])
            schema_data = []
            @schemas.map do |schema|
              schema_data << {sku: schema, allocated_to: schema.allocated_to, allocated_by: schema.allocated_by}
            end
            render json: schema_data
        else
            render json: @schemas.paginate(page: params[:page], per_page: params[:per_page])
        end
    else
        render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # GET /schemas_pivot
  def pivot_view
    project = Project.find_by(db_name: params[:db_name])
    if project.present?  
      project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
      @schemas = Schema.where(project_file_id: project_file._id)
      @pivot_data = {}
      column_names = params[:column_names].present? ? params[:column_names].split(".") : nil
      if column_names.present?
	column_names.map{|i| @pivot_data[i] = @schemas.pluck(i).uniq}
      else
        project_file.actual_headers.map{|i| @pivot_data[i] = @schemas.pluck(i).uniq}
      end
      render json: @pivot_data
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end

  # GET /schema_details
  def schema_details
    project = Project.find_by(db_name: params[:db_name])
    if project.present?
      project.tenancy!
      project_file = ProjectFile.find(params[:file_id]) rescue nil
             
      if params[:search_key].present? && params[:search_key].length > 0
	  search_strategy = params[:search_strategy] == "contains" ? "any" : "all"
#	  search_strategy = "all"
          @schemas = Schema.full_text_search(params[:search_key], match: search_strategy)
          @schemas = @schemas.where(project_file_id: project_file._id) if project_file.present? && @schemas.present?
      else
          @schemas = Schema.where(project_file_id: project_file._id) if project_file.present?    
      end
      column_values = params[:column_values].split(".") if params[:column_values].present?
      @schemas = params[:column_name].present? && column_values.present? ? @schemas.in(params[:column_name] => column_values) : @schemas
      @schemas = @schemas.where(params[:filter_column] => params[:filter_value]) if params[:filter_column].present? && params[:filter_value].present?
      schema_ids = @schemas.pluck(:_id) if @schemas.present?

      allocated_count = @schemas.where(:allocated_to_id.ne => nil).count
      unallocated_count = @schemas.where(allocated_to_id: nil).count
      render json: {status: true, total_count: @schemas.count, allocated: allocated_count, unallocated: unallocated_count, columns: project_file.actual_headers.uniq}    
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # GET /schema_files
  def schema_files
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?  
      @project.tenancy!
      content_process = ContentProcess.find_by(name: "Schema") rescue nil
      file_process_mappings = FileProcessMapping.where(content_process_id: content_process.id)
      file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
      @project_files = file_ids.present? ? ProjectFile.where(:id.in => file_ids) : []
      render json: @project_files
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # GET /schemas/1
  def show
    render json: @schema
  end

  # POST /schemas
  def create
    @schema = Schema.new(schema_params)

    if @schema.save
      render json: @schema, status: :created, location: @schema
    else
      render json: @schema.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /schemas/1
  def update
    if @schema.update(schema_params)
      render json: @schema
    else
      render json: @schema.errors, status: :unprocessable_entity
    end
  end

  # DELETE /schemas/1
  def destroy
    @schema.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schema
      @schema = Schema.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def schema_params
      params.require(:schema).permit! #(:project_file_id, :status, :created_at, :updated_at)
    end
end


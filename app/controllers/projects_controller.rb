class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :update, :destroy]

  # GET /projects
  def index
    filter_query = {}
    request.query_parameters.keys.map{|i| filter_query[i] = params[i] if Project.attribute_names.include?(i)}
    
    if @current_user.is_QA_admin?
      @projects = Project.projects
    else
      @projects = filter_query != {} ? Project.where(filter_query) : Project.all
      @projects = @projects.full_text_search(params[:search_key]) if params[:search_key].present? && params[:search_key].length > 0
      @projects = @projects.order("#{params[:sort_key]} #{params[:sort_order]}") if (params[:sort_key].present? && Project.attribute_names.include?(params[:sort_key])) && params[:sort_order].present?
    end 
    render json: @projects #.paginate(page: params[:page], per_page: params[:per_page])
  end

  def trashed_projects
	filter_query = {}
    request.query_parameters.keys.map{|i| filter_query[i] = params[i] if Project.attribute_names.include?(i)}
    @projects = filter_query != {} ? Project.deleted.where(filter_query) : Project.deleted
    @projects = @projects.full_text_search(params[:search_key]) if params[:search_key].present? && params[:search_key].length > 0
    @projects = @projects.order("#{params[:sort_key]} #{params[:sort_order]}") if (params[:sort_key].present? && Project.attribute_names.include?(params[:sort_key])) && params[:sort_order].present?
    render json: @projects #.paginate(page: params[:page], per_page: params[:per_page])
  end

  # GET /projects/1
  def show
    render json: @project
  end

  # POST /projects
  def create
    @project = Project.new(project_params)

    if @project.save
      render json: @project, status: :created
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /projects/1
  def update
    if @project.update(project_params)
      render json: @project
    else
      render json: @project.errors, status: :unprocessable_entity
    end
  end

  # DELETE /projects/1
  def destroy
    if @project.destroy
	render json: {status: true, message: "Deleted Successfully"}
    else
	render json: {status: false, message: "Something went wrong. Please try again"}
    end 
  end

  # GET /restore_project/1
  def restore_project
     @project = Project.deleted.find(params[:id]) rescue nil
     if @project.present? && @project.restore
	render json: {status: true, message: "Project restored successfully"}
     else
	render json: {status: false, message: "Something went wrong. Try again!"}
     end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

     # Only allow a trusted parameter "white list" through.
    def project_params
      params.require(:project).permit(:name, :db_name, :project_code, :project_type_id, :client_type_id, :client_logo, :region_id, :service_id, :deadline, :status, :created_by_id, project_process_mappings_attributes: [:id, :content_process_id, :deadline])
    end

end

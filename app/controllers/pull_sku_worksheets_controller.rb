class PullSkuWorksheetsController < ApplicationController
    
	include WorksheetLog # To include the worksheet_log concern

	# POST - /pull_sku_worksheets
	def pull_sku_data

		@project = Project.find_by(db_name: params[:db_name])

		if @project.present?
			
			@project.tenancy!

			project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?
			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?

			@user_worksheet_log = UserWorksheetLog.find_by(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:user_id]) rescue nil

			if @user_worksheet_log.present?

				# Authenticate a session with your Service Account
				session = GoogleDrive::Session.from_service_account_key("client_secret.json")
				# Get the first worksheet
				spreadsheet = session.spreadsheet_by_key(@user_worksheet_log.sheet_id)
				worksheet = spreadsheet.worksheets.first
				sheet_data = worksheet.rows
				column_names = sheet_data[0]
				# new_header_values = project_file.actual_headers - (column_names - ["id"])
				# total_header_values = project_file.actual_headers + new_header_values
				new_header_values = column_names - ["id"] if column_names.present?
				project_file.update(actual_headers: new_header_values.uniq) if new_header_values.present?

				sheet_data[1..-1].map do |data|
					updatable_data = {}
					column_names.each_with_index do |col_name, index|
						unless project_file.input_headers.include?(col_name)
							updatable_data[col_name] = data[index]
						end
					end
					@sku = Sku.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
					if @sku.present?
						@sku.update(updatable_data)
						@allocation = Allocation.find_by(sku_id: @sku.id, content_process_id: content_process.id) if content_process.present?
						status = data[column_names.index("#{params[:content_process_name]}_status")]
						@allocation.update(status: status) if @allocation.present?
					end
				end
				# To delete the content producer worksheet activity when the activity gets completed
				delete_worksheet_log(@project.id, project_file.id, content_process.id, params[:user_id], @user_worksheet_log.sheet_id)
				render json: {status: true, message: "Data saved Successfully"}
			else
				render json: {status: false, message: "No WorkLog Found"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
		
	end


end




 #    def view_user_data

	# 	@project = Project.find_by(db_name: params[:db_name])

	# 	if @project.present?
			
	# 		@project.tenancy!

	# 		project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?
	# 		content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?

	# 		@user_worksheet_log = UserWorksheetLog.find_by(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:user_id]) rescue nil

	# 		if @user_worksheet_log.present?

	# 			# Authenticate a session with your Service Account
	#                         #session = GoogleDrive::Session.from_service_account_key("client_secret.json")
	#                                 # Get the first worksheet
	#                         #spreadsheet = session.spreadsheet_by_key(@user_worksheet_log.sheet_id)
	#                         #worksheet = spreadsheet.worksheets.first

	# 			# Authenticate a session with your Service Account
	# 			# session = GoogleDrive::Session.from_service_account_key("client_secret.json")

	# 	   		@drive = Google::Apis::DriveV3::DriveService.new
	# 			@drive.authorization = GoogleDrive::Session.from_service_account_key("client_secret.json")
	# byebug
	# 			user_permission = {type: "user", role: "writer", email_address: @current_user.email_id}
	# #			@sheet = @drive.spreadsheet_by_key(@user_worksheet_log.sheet_id)
	# #			@sheet.add_request({create_permission: {file_id: @user_worksheet_log.sheet_id, body: user_permission, fields: 'id', send_notification_email: true}})
	# #			@sheet.save
	# 			@drive.create_permission(@user_worksheet_log.sheet_id, user_permission, send_notification_email: true, fields: 'id')
	# 			#session.create_permission(@user_worksheet_log.sheet_id, user_permission, send_notification_email: true, fields: 'id')

	# 			# Get the first worksheet
	# 			# spreadsheet = session.spreadsheet_by_key(@user_worksheet_log.sheet_id)
	# 			# worksheet = spreadsheet.worksheets.first
	# 			# sheet_data = worksheet.rows
	# 			# column_names = sheet_data[0]
	# 			# new_header_values = project_file.actual_headers - (column_names - ["id"])
	# 			# total_header_values = project_file.actual_headers + new_header_values
	# 			# new_header_values = column_names - ["id"] if column_names.present?
	# 			# project_file.update(actual_headers: new_header_values.uniq) if new_header_values.present? 
				
	# 			# sheet_data[1..-1].map do |data|
	# 			# 	updatable_data = {}
	# 			# 	column_names.each_with_index do |col_name, index|
	# 			# 		unless project_file.input_headers.include?(col_name)
	# 			# 			updatable_data[col_name] = data[index]
	# 			# 		end
	# 			# 	end
	# 			# 	@sku = Sku.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
	# 			# 	if @sku.present?
	# 			# 		@sku.update(updatable_data)
	# 			# 		@allocation = Allocation.find_by(sku_id: @sku.id, content_process_id: content_process.id) if content_process.present?
	# 			# 		status = data[column_names.index("#{params[:content_process_name]}_status")]
	# 			# 		@allocation.update(status: status) if @allocation.present?
	# 			# 	end
	# 			# end
	# 			# To delete the content producer worksheet activity when the activity gets completed
	# 			# delete_worksheet_log(@project.id, project_file.id, content_process.id, params[:user_id], @user_worksheet_log.sheet_id)
	# 			render json: {status: true, message: "Sheet shared Successfully"}
	# 		else
	# 			render json: {status: false, message: "No WorkLog Found"}
	# 		end
	# 	else
	# 		render json: {status: false, message: "Invalid Project Code"}
	# 	end	
	# end
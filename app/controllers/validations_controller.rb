class ValidationsController < ApplicationController

	# GET - To validate the uniqueness of a project name
	def validate_project_name
		@project = Project.unscoped.find_by(name: params[:project_name]) rescue nil
		if @project.present?
			render json: {status: false, message: "Project Name already Taken"}
		else
			render json: {status: true, message: "Project Name validated successfully"}
		end
	end

	# GET - To validate the uniqueness of a project code
	def validate_project_code
		@project = Project.unscoped.find_by(project_code: params[:project_code]) rescue nil
		if @project.present?
			render json: {status: false, message: "Project Code already Taken"}
		else
			render json: {status: true, message: "Project Code validated successfully"}
		end
	end

	def validate_username
		@user = User.find_by(username: params[:username]) rescue nil
		if @user.present?
			render json: {status: false, message: "Username already Taken"}
		else
			render json: {status: true, message: "Username validated successfully"}
		end
	end

	def validate_email_id
		@user = User.find_by(email_id: params[:email_id]) rescue nil
		if @user.present?
			render json: {status: false, message: "Email Id already Taken"}
		else
			render json: {status: true, message: "Email Id validated successfully"}
		end
	end

	# GET - To validate the uniqueness of a repository name
	def validate_repository_name
		@repository = FileRepository.find_by(project_name: params[:project_name]) rescue nil
		@project = Project.find_by(name: params[:project_name]) rescue nil
		if @repository.present? || @project.present?
			render json: {status: false, message: "Project Name already Taken"}
		else
			render json: {status: true, message: "Project Name validated successfully"}
		end
	end


end

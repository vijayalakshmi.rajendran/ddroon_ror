require 'will_paginate/mongoid'
class ApplicationController < ActionController::API

	before_action :authorize_request

#      skip_before_action :authorize_request, only: :sidekiq
	# serialization_scope :authorize_request

	 # For rendering 400 error message when the Login Token is invalid
    def not_found
    	render json: { error: 'not_found' }
    end

  	# For validating the token
    def authorize_request
    	header = request.headers['Authorization']
    	header = header.split(' ').last if header
    	begin
      		@decoded = JsonWebToken.decode(header)
      		@current_user = User.find(@decoded[:user_id]) rescue nil         
    	rescue JWT::DecodeError => e
      		render json: { errors: e.message }, status: :unauthorized
        rescue => e
                render json: { errors: e.message }, status: :unauthorized		
    	end
  	end

end

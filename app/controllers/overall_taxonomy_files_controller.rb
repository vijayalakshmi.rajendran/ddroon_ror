class OverallTaxonomyFilesController < ApplicationController
  skip_before_action :authorize_request, only: :index


   def index
                response = []
                content_process = ContentProcess.find_by(name: "Taxonomy") rescue nil
                Project.all.map do |project|
                  project.tenancy!
                  file_process_mappings = FileProcessMapping.where(content_process_id: content_process.id)
                  #file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
                  #file_data = file_ids.present? ? ProjectFile.where(:id.in => file_ids) : []
		  file_data = []
		  file_process_mappings.map do |fpm|
                     file_data << fpm.project_file if fpm.project_file.present?
		  end
                  response << {project_id: project.id, project_name: project.name, db_name: project.db_name, deadline: project.deadline, content_process_id: content_process.id, taxonomy_files: file_data}
                end
                render json: response
    end



  # GET /overall_taxonomy_files
  def backup_index
        	response = []
		content_process = ContentProcess.find_by(name: "Taxonomy") rescue nil
		Project.all.map do |project|
                  project.tenancy!
                  file_process_mappings = FileProcessMapping.where(content_process_id: content_process.id)
		  file_ids = file_process_mappings.pluck(:project_file_id) if file_process_mappings.present?
		  file_data = file_ids.present? ? ProjectFile.where(:id.in => file_ids) : []
                  response << {project_id: project.id, project_name: project.name, taxonomy_files: file_data}
                end
                render json: response
    end
end

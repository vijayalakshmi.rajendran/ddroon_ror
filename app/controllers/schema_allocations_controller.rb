require 'fcm'

class SchemaAllocationsController < ApplicationController

  # GET /schema_allocations
  def index
    @project = Project.find_by(db_name: params[:db_name])
    if @project.present?
      @project.tenancy!
      project_file = ProjectFile.find(params[:file_id])
      @schemas = Schema.where(project_file_id: project_file.id)
      column_values = @schemas.pluck(params[:column_name]).uniq
      allocation_data = {}
      column_values.map do |i|
        @columwise_schemas = @schemas.where(params[:column_name] => i)
        allocated_schemas = @columwise_schemas.where(:allocated_to_id.ne => nil)
        allocated_schema_data = allocated_schemas.present? ? allocated_schemas.map{|i| {sku_id: i.id, allocated_to: i.allocated_to}} : []
        unallocated_schemas = @columwise_schemas.present? && @columwise_schemas.where(allocated_to_id: nil).present? ? @columwise_schemas.where(allocated_to_id: nil).pluck(:_id) : []
        user_data = {}
        user_schemas = allocated_schemas.group_by{|i| i.allocated_to} if allocated_schemas.present?
        user_schemas.map{|k, v| user_data[k.username] = v.map{|i| i._id}} if user_schemas.present?
        allocation_data[i] = {allocated_skus: allocated_schema_data, unallocated_skus: unallocated_schemas, user_data: user_data}
      end
      total_allocated_count = @schemas.where(:allocated_to_id.ne => nil).count
      render json: {allocation: allocation_data, allocated: total_allocated_count, unallocated: @schemas.count - total_allocated_count}
    else
      render json: {status: false, message: "Invalid Project Code"}
    end
  end


  # POST /schema_allocations
  def create
   	@project = Project.find_by(db_name: params[:db_name]) rescue nil
    if @project.present?
      @project.tenancy!
	    @content_producer = User.find(params[:content_producer_id]) if params[:content_producer_id].present?
      content_producer_id = @content_producer.id if @content_producer.present?
#      content_producer_id = User.find(params[:content_producer_id]).id if params[:content_producer_id].present?
      admin = User.find(params[:admin_id]) if params[:admin_id].present?
      project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
      begin
	      params[:ids].map do |i| 
	        schema = Schema.find(i) rescue nil
	        if schema.present?
	           content_producer_id.present? ? schema.update(allocated_to_id: content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id) : schema.update(allocated_to_id: nil, allocated_by_id: nil, project_file_id: project_file.id)
	        end
	      end
		    
        if @content_producer.present? && @content_producer.fcm_token.present?
            fcm = FCM.new(ENV["FCM_SERVER_KEY"])
            registration_ids= [@content_producer.fcm_token] # an array of one or more client registration tokens
            options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
            fcm.send(registration_ids, options)
        end
	      render json: {status: true, message: "Allocated successfully"}, status: :ok
  	  rescue
      	  render json: {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
      end  
    else
        render json: {status: false, message: "Invalid Project Code"}
    end
  end

   # POST /multiple_schema_allocation
  def multiple_schema_allocation
      @project = Project.find_by(db_name: params[:db_name]) rescue nil
      if @project.present?
        @project.tenancy!
	      registration_ids = []
      #      content_producer_id = User.find(params[:content_producer_id]).id if params[:content_producer_id].present?
        admin = User.find(params[:admin_id]) if params[:admin_id].present?
        project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
        begin
          params[:allocation_data].map do |data|
            data[:schema_ids].map do |i|
                schema = Schema.find(i) rescue nil
                if schema.present?
		                @content_producer = User.find(data[:content_producer_id]) if data[:content_producer_id].present?
                    if @content_producer.present? && @content_producer.fcm_token.present?
                      registration_ids << @content_producer.fcm_token unless registration_ids.include?(@content_producer.fcm_token) # an array of one or more client registration tokens
                    end
                   data[:content_producer_id].present? ? schema.update(allocated_to_id: @content_producer.id, allocated_by_id: admin.id, project_file_id: project_file.id) : schema.update(allocated_to_id: nil, allocated_by_id: nil, project_file_id: project_file.id)
                end
            end
          end
          if registration_ids != []
            fcm = FCM.new(ENV["FCM_SERVER_KEY"])
            options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
            fcm.send(registration_ids, options)
          end
         render json: {status: true, message: "Allocated successfully"}, status: :ok
        rescue
          render json: {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
        end
      else
        render json: {status: false, message: "Invalid Project Code"}
      end
  end



end

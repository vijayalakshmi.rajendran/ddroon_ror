class ViewUserWorksheetsController < ApplicationController

	# POST - /view_user_worksheets  
    def index
		@project = Project.find_by(db_name: params[:db_name])
		if @project.present?			
			@project.tenancy!
			project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?
			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?
			@user_worksheet_log = UserWorksheetLog.find_by(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:user_id]) rescue nil
			if @user_worksheet_log.present?			
				# Authenticate a session with your Service Account
				session = GoogleDrive::Session.from_service_account_key("client_secret.json")
				# Get the first worksheet
				user_spreadsheet = session.spreadsheet_by_key(@user_worksheet_log.sheet_id)
				user_worksheet = user_spreadsheet.worksheets.first
				sheet_data = user_worksheet.rows
				sheet_headers = sheet_data[0]

				new_spreadsheet = session.spreadsheet_by_key(params[:new_sheet_id])
				# Get the first worksheet
				new_worksheet = new_spreadsheet.worksheets.first

				# Dumps all cells.
				(1..new_worksheet.num_rows).each do |row|
				  (1..new_worksheet.num_cols).each do |col|
				    new_worksheet[row, col] = ""
				  end
				end

	            new_worksheet.add_request({
			        delete_dimension: {
			            range: {
			              sheet_id: new_worksheet.sheet_id,
			              dimension: "COLUMNS",
			              start_index: 1,
			              end_index: 26
			            }
			        }
			    })

				new_worksheet.insert_rows(1, [sheet_headers])
				new_worksheet.save
				new_worksheet.insert_rows(new_worksheet.rows.count+1, sheet_data[1..-1])
				new_worksheet.save

#				@user_worksheet_log.update(sheet_id: params[:new_sheet_id])

				render json: {status: true, message: "Data sent Successfully"}
			else
				render json: {status: false, message: "No WorkLog Found"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end	
	end


end	

require 'fcm'

class SchemaSheetAllocationsController < ApplicationController

	# GET - /schema_sheet_allocations
	def send_data_for_allocation
		@project = Project.find_by(db_name: params[:db_name]) rescue nil

		if @project.present?
			@project.tenancy!
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			# Get the first worksheet
			worksheet = spreadsheet.worksheets.first

			content_process = ContentProcess.find(params[:content_process_id]) rescue nil
			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
			sheet_headers = ["id"] + project_file.actual_headers + ["#{content_process.name}_by"]

			# Dumps all cells.
			(1..worksheet.num_rows).each do |row|
			  (1..worksheet.num_cols).each do |col|
			    worksheet[row, col] = ""
			  end
			end
			worksheet.insert_rows(1, [sheet_headers])
			worksheet.save

			@schemas = Schema.where(project_file_id: params[:file_id])
			sheet_data = []
			@schemas.map do |schema|
				schema_data = sheet_headers.map do |i| 
				  if i == "#{content_process.name}_by"
				      schema.allocated_to.present? ? schema.allocated_to.username : nil
				  else
				     schema[i]
				  end
				end
				sheet_data << schema_data
			end

			worksheet.insert_rows(worksheet.rows.count+1, sheet_data)
			worksheet.save

			# To hide the id field column in the sheet for end_user
			first_worksheet = spreadsheet.worksheets.first
			sheet_id = first_worksheet.sheet_id

			first_worksheet.add_request(
			  {
			    add_protected_range: {
			      protected_range: {
			        range: {
			          sheet_id: sheet_id,
			          # start_row_index: 0,
			          # end_row_index: 1,
			          start_column_index: 0,
			          end_column_index: 1,
			        },
			        description: 'Protecting total row',
			        warning_only: true,
			        requesting_user_can_edit: false,
			      }
			    }
			  }
			)

			first_worksheet.save

			# To protect the id field column cells from editing by mistake
			first_worksheet.add_request(
				{
				  	update_dimension_properties: {
					    range: {
					      sheet_id: sheet_id,
					      dimension: 'COLUMNS',
					      start_index: 0,
					      end_index: 1,
					    },
					    properties: {
					      hidden_by_user: true,
					    },
					    fields: 'hidden_by_user',
					}
				}
			)

			first_worksheet.save


			render json: {status: true, message: "Data Sent Successfully"}
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end
	

    # POST - /schema_sheet_allocations
	def save_allocation_data
		@project = Project.find_by(db_name: params[:db_name]) rescue nil
		if @project.present?
			@project.tenancy!

			registration_ids = []
			# Authenticate a session with your Service Account
			session = GoogleDrive::Session.from_service_account_key("client_secret.json")
			# Get the first worksheet
			spreadsheet = session.spreadsheet_by_key(params[:sheet_id])
			worksheet = spreadsheet.worksheets.first
			sheet_data = worksheet.rows
			column_names = sheet_data[0]

			content_process = ContentProcess.find(params[:content_process_id]) if params[:content_process_id].present?
			project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
			admin = User.find(params[:admin_id]) if params[:admin_id].present?

			begin 
				sheet_data[1..-1].map do |data|
					@schema = Schema.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
					# updatable_data["sku_id"] = @taxonomy.id if @taxonomy.present?
					if @schema.present?
						updatable_data = {} 
						content_producer = User.find_by(username: data[column_names.index("#{content_process.name}_by")]) if column_names.index("#{content_process.name}_by").present? && data[column_names.index("#{content_process.name}_by")].present?
						# registration_ids << content_producer.fcm_token if content_producer.present? && content_producer.fcm_token.present?
						updatable_data["allocated_to_id"] = content_producer.id if content_producer.present?
						updatable_data["allocated_by_id"] = admin.id if admin.present? && content_producer.present?
						updatable_data["project_file_id"] = project_file.id if project_file.present?
						if content_producer.present? && content_producer.fcm_token.present?
	                		registration_ids << content_producer.fcm_token unless registration_ids.include?(content_producer.fcm_token) && @schema.allocated_to_id != content_producer.id
			        	end
						@schema.update_attributes(updatable_data)
					end
				end	
				if registration_ids != []
					fcm = FCM.new(ENV["FCM_SERVER_KEY"])
					options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}	
					fcm.send(registration_ids, options)
				end
				render json: {status: true, message: "Allocation Successfull"}
			rescue => error
				render json: {status: false, message: "Something went wrong! Please try again"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
	end


end

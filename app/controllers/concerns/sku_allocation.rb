#require 'fcm'

module SkuAllocation
  extend ActiveSupport::Concern

  	def allocation_summary(db_name, file_id, column_name, content_process_id)
	  	@project = Project.find_by(db_name: db_name)
	    if @project.present?
		    @project.tenancy!
		    project_file = ProjectFile.find(file_id)
		    @skus = Sku.where(project_file_id: project_file.id)		    
		    sku_ids = @skus.pluck(:_id)  #only(:_id).map(&:_id)
		    @allocations = Allocation.where(:sku_id.in => sku_ids, :content_process_id => content_process_id)
		    column_values = @skus.pluck(column_name).uniq
		    allocation_data = {}
		    column_values.map do |i|
		        column_sku_ids = @skus.where(column_name => i).pluck(:id) if @skus.where(column_name => i).present?
		        # allocated_skus = Allocation.where(:sku_id.in => column_sku_ids, :content_process_id => content_process_id)
		        allocated_skus = @allocations.where(:sku_id.in => column_sku_ids) if @allocations.present?
		        allocated_skus = allocated_skus.not(allocated_to_id: nil) if allocated_skus.present?
			    allocated_sku_data = allocated_skus.present? ? allocated_skus.map{|i| {sku_id: i.sku_id, allocated_to: i.allocated_to}} : []
		        allocated_sku_ids = allocated_skus.present? ? allocated_skus.pluck(:sku_id) : []
		        unallocated_skus = column_sku_ids - allocated_sku_ids
		        user_data = {}
		        user_skus = allocated_skus.group_by{|i| i.allocated_to} if allocated_skus.present?
		        user_skus.map{|k, v| user_data[k.username] = v.map{|i| i.sku_id} if k.present?} if user_skus.present?
		        allocation_data[i] = {allocated_skus: allocated_sku_data, unallocated_skus: unallocated_skus, user_data: user_data}
		    end
	       # total_allocated_count = Allocation.where(:sku_id.in => sku_ids, :content_process_id => content_process_id).count
	       total_allocated_count = @allocations.count
	       return {allocation: allocation_data, allocated: total_allocated_count, unallocated: @skus.count - total_allocated_count}
	    else
	       return {status: false, message: "Invalid Project Code"}
	    end
  	end


  	def allocate_skus(db_name, content_process_id, file_id, content_producer_id, admin_id, ids)
  		@project = Project.find_by(db_name: db_name) rescue nil
	    if @project.present?
	        @project.tenancy!	      
	        content_process = ContentProcess.find(content_process_id) if content_process_id.present?
	        @content_producer = User.find(content_producer_id) if content_producer_id.present?
			@content_producer_id = @content_producer.id if @content_producer.present?
	        admin = User.find(admin_id) if admin_id.present?
	        project_file = ProjectFile.find(file_id) if file_id.present?
	        allocation_data = []
                sku_ids = ids.map{|i| BSON.ObjectId(i)}
	        @allocations = Allocation.where(:sku_id.in => sku_ids).where(project_file_id: project_file.id, content_process_id: content_process.id) if sku_ids.present?
	        sku_ids.map do |i| 
		        # allocation = Allocation.where(sku_id: Sku.find(i).id, content_process_id: content_process.id, project_file_id: project_file.id)
		        allocation = @allocations.find_by(sku_id: i) rescue nil
		        if allocation.present?
		           @content_producer_id.present? ? allocation.update(sku_id: i, content_process_id: content_process.id, allocated_to_id: @content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id) : allocation.delete
		        else
		           allocation_data << {sku_id: i, content_process_id: content_process.id, allocated_to_id: @content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id }
		        end
	      	end
	      	if Allocation.collection.insert_many(allocation_data)
				if @content_producer.present? && @content_producer.fcm_token.present?
	              fcm = FCM.new(ENV["FCM_SERVER_KEY"])
	              registration_ids= [@content_producer.fcm_token] # an array of one or more client registration tokens
	              options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
	              fcm.send(registration_ids, options)
	            end
	        	return {status: true, message: "Allocated successfully"}, status: :ok
	      	else
	        	return {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
	      	end
	    else
	      	return {status: false, message: "Invalid Project Code"}
	    end
  	end



	def allocate_skus_to_multiple_users(db_name, content_process_id, file_id, admin_id, allocation_data)
            @project = Project.find_by(db_name: db_name) rescue nil
            if @project.present?
                @project.tenancy!
				registration_ids = []
                content_process = ContentProcess.find(content_process_id) if content_process_id.present?
                admin = User.find(admin_id) if admin_id.present?
                project_file = ProjectFile.find(file_id) if file_id.present?
	   			new_allocation_data = []
	   			user_ids = allocation_data.collect{|alloc_data| alloc_data[:content_producer_id]}
	   			@content_producers = User.where(:id.in => user_ids) if user_ids.present? && user_ids.size > 0
                allocation_data.map do |data|
			 	    content_producer = @content_producers.find(data[:content_producer_id]) if data[:content_producer_id].present?
				    content_producer_id = content_producer.id if content_producer.present?
				    if content_producer.present? && content_producer.fcm_token.present?
				   		registration_ids << content_producer.fcm_token unless registration_ids.include?(content_producer.fcm_token) # an array of one or more client registration tokens
		 		    end
				    skus_ids_arr = data[:skus_ids].map{|i| BSON.ObjectId(i)}
		 		    @allocations = Allocation.where(:sku_id.in => skus_ids_arr).where(content_process_id: content_process.id, project_file_id: project_file.id) if skus_ids_arr.present?
		            skus_ids_arr.map do |i|
#                                sku_id = BSON.ObjectId(i)
		                allocation = @allocations.find_by(sku_id: i) rescue nil
		                if allocation.present?
		                    content_producer_id.present? ? allocation.update(sku_id: i, content_process_id: content_process.id, allocated_to_id: content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id) : allocation.delete
		                else
		                    new_allocation_data << {sku_id: i, content_process_id: content_process.id, allocated_to_id: content_producer_id, allocated_by_id: admin.id, project_file_id: project_file.id }
		                end
				    end
                end
                if Allocation.collection.insert_many(new_allocation_data)
					if registration_ids != []
						fcm = FCM.new(ENV["FCM_SERVER_KEY"])
                       	options = { "notification": {"title": "DDROON", "body": "Hi, New Skus has been allocated to you. Refresh the screen to view it."}}
                        fcm.send(registration_ids, options)
                    end
                    return {status: true, message: "Allocated successfully"}, status: :ok
                else
                    return {status: false, message: "Something went wrong. Please try again"}, status: :unprocessable_entity
                end
            else
                return {status: false, message: "Invalid Project Code"}
            end
    end




end

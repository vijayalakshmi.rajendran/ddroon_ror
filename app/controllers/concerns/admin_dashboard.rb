module AdminDashboard
	extend ActiveSupport::Concern

	def projects_summary
		# return Project.where(status: "ACTIVE").map{|prj| { project_id: prj._id, db_name:prj.db_name, project_name: prj.name, deadline: prj.deadline }}
		
		@current_user.is_QA_admin? ? @projects = Project.projects : @projects = Project.where(status: "ACTIVE")
		response_data = []
		@projects.map do |project|
			project.tenancy!
			content_process_ids = FileProcessMapping.all.pluck(:content_process_id) if FileProcessMapping.all.present?
			content_processes = content_process_ids.present? ? ContentProcess.where(:id.in => content_process_ids).pluck(:name) : []
    #   content_processes.map do |content_process|
				# # file_ids = content_process.file_process_mappings.pluck(:project_file_id) if content_process.file_process_mappings.present?
				# # @project_files = ProjectFile.in(id: file_ids) if file_ids.present?
				# # completed_values = content_process.status_values.present? ? content_process.status_values[:positive] : []
				# # if @project_files.present?
				# # 	file_arr = []
				# # 	@project_files.map do |project_file|
				# # 	  total_sku_count = project_file.skus.present? ? project_file.skus.count : 0
				# # 	  if total_sku_count > 0
    # #           allocated_skus = project_file.allocations.where(content_process_id: content_process.id) if project_file.allocations.present?
				# # 			allocated_sku_count = allocated_skus.present? ? allocated_skus.count : 0
				# # 			unallocated_sku_count = total_sku_count - allocated_sku_count
				# # 			unallocated_sku_count = unallocated_sku_count < 0 ? 0 : unallocated_sku_count
				# # 			completed_sku_count = allocated_skus.present? && allocated_skus.where(:status.in => completed_values).present? ? allocated_skus.where(:status.in => completed_values).count : 0
				# # 			remaining_sku_count = allocated_sku_count.present? ? allocated_sku_count - completed_sku_count : 0
				# # 			file_data = {file_name: project_file.file_name, deadline: project_file.deadline, total: total_sku_count, allocated: allocated_sku_count, unallocated: unallocated_sku_count, completed: completed_sku_count, remaining: remaining_sku_count}
				# # 			statuswise_data = allocated_skus.group_by(&:status) if allocated_skus.present?
				# # 			statuswise_report = {}
				# # 			statuswise_data.map{|k, v| k != nil && k.length > 0 ? statuswise_report[k] = v.count : statuswise_report["Yet to start"] = v.count} if statuswise_data.present?
				# # 			file_data["statuswise_report"] = statuswise_report
				# # 			file_arr << file_data
    # #         end
				# # 	end
					#processwise_data << content_process.name]# = {content_process_id: content_process.id, file_data: file_arr}
				#end
			#end
			response_data << { project_id: project._id, db_name:project.db_name, project_name: project.name, deadline: project.deadline, processes: content_processes }
		end
		return response_data
	end

  def completed_projects_summary
		@projects = Project.where(status: "COMPLETED")
		response_data = []
		@projects.map do |project|
			project.tenancy!
			processwise_data = {}
			content_process_ids = FileProcessMapping.all.pluck(:content_process_id) if FileProcessMapping.all.present?
			content_processes = content_process_ids.present? ? ContentProcess.where(:id.in => content_process_ids) : []
            content_processes.map do |content_process|
				file_ids = content_process.file_process_mappings.pluck(:project_file_id) if content_process.file_process_mappings.present?
				@project_files = ProjectFile.in(id: file_ids) if file_ids.present?
				completed_values = content_process.status_values.present? ? content_process.status_values[:positive] : []
				if @project_files.present?
					file_arr = []
					@project_files.map do |project_file|
					    total_sku_count = project_file.skus.present? ? project_file.skus.count : 0
					    if total_sku_count > 0
                            				allocated_skus = project_file.allocations.where(content_process_id: content_process.id) if project_file.allocations.present?
							allocated_sku_count = allocated_skus.present? ? allocated_skus.count : 0
							unallocated_sku_count = total_sku_count - allocated_sku_count
							unallocated_sku_count = unallocated_sku_count < 0 ? 0 : unallocated_sku_count
							completed_sku_count = allocated_skus.present? && allocated_skus.where(:status.in => completed_values).present? ? allocated_skus.where(:status.in => completed_values).count : 0
							remaining_sku_count = allocated_sku_count.present? ? allocated_sku_count - completed_sku_count : 0
							file_data = {file_name: project_file.file_name, deadline: project_file.deadline, total: total_sku_count, allocated: allocated_sku_count, unallocated: unallocated_sku_count, completed: completed_sku_count, remaining: remaining_sku_count}
							statuswise_data = allocated_skus.group_by(&:status) if allocated_skus.present?
							statuswise_report = {}
							statuswise_data.map{|k, v| k != nil && k.length > 0 ? statuswise_report[k] = v.count : statuswise_report["Yet to start"] = v.count} if statuswise_data.present?
							file_data["statuswise_report"] = statuswise_report
							file_arr << file_data
                        		    end
					end
					processwise_data[content_process.name] = {content_process_id: content_process.id, file_data: file_arr}
				end
			end
			response_data << {project_id: project.id, project_name: project.name, deadline: project.deadline, processwise_data: processwise_data}
		end
		return response_data
	end

	def single_project_summary(project)
		project.tenancy!
		processwise_data = {}
			content_process_ids = FileProcessMapping.all.pluck(:content_process_id) if FileProcessMapping.all.present?
			content_processes = content_process_ids.present? ? ContentProcess.where(:id.in => content_process_ids) : []
      		content_processes.map do |content_process|
				file_ids = content_process.file_process_mappings.pluck(:project_file_id) if content_process.file_process_mappings.present?
				if @current_user.is_QA_admin?
					@project_files = file_ids.present? ? ProjectFile.where(:id.in => file_ids, :audit_stage => "inAudit") : []
				else
					@project_files = ProjectFile.in(id: file_ids) if file_ids.present?
				end
				completed_values = content_process.status_values.present? ? content_process.status_values[:positive] : []
				if @project_files.present?
					file_arr = []
					@project_files.map do |project_file|
					  total_sku_count = project_file.skus.present? ? project_file.skus.count : 0
					  if total_sku_count > 0
              			allocated_skus = project_file.allocations.where(content_process_id: content_process.id) if project_file.allocations.present?
							allocated_sku_count = allocated_skus.present? ? allocated_skus.count : 0
							unallocated_sku_count = total_sku_count - allocated_sku_count
							unallocated_sku_count = unallocated_sku_count < 0 ? 0 : unallocated_sku_count
							completed_sku_count = allocated_skus.present? && allocated_skus.where(:status.in => completed_values).present? ? allocated_skus.where(:status.in => completed_values).count : 0
							remaining_sku_count = allocated_sku_count.present? ? allocated_sku_count - completed_sku_count : 0
							file_data = {file_name: project_file.file_name, deadline: project_file.deadline, total: total_sku_count, allocated: allocated_sku_count, unallocated: unallocated_sku_count, completed: completed_sku_count, remaining: remaining_sku_count}
							statuswise_data = allocated_skus.group_by(&:status) if allocated_skus.present?
							statuswise_report = {}
							statuswise_data.map{|k, v| k != nil && k.length > 0 ? statuswise_report[k] = v.count : statuswise_report["Yet to start"] = v.count} if statuswise_data.present?
							file_data["statuswise_report"] = statuswise_report
							file_arr << file_data
            end
					end
					processwise_data[content_process.name] = {content_process_id: content_process.id, file_data: file_arr}
				end
			end
			return processwise_data
	end

end

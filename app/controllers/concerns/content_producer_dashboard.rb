module ContentProducerDashboard
	extend ActiveSupport::Concern

	def projectwise_allocations
		@projects = Project.all
		project_details = {}
		total_skus_arr = []
		completed_skus_arr = []
		remaining_skus_arr = []
		@projects.map do |project|			
			data = {}
			projectwise_total_skus_arr = []
			projectwise_completed_skus_arr = []
			projectwise_remaining_skus_arr = []
			content_processes = project.project_process_mappings.present? ? project.project_process_mappings.map{|i| i.content_process} : []
            content_processes.map do |content_process|		
				positive_values = content_process.status_values.present? ? content_process.status_values[:positive] : []
	            completed_values = positive_values.present? ? positive_values : [] 	
				if content_process.name == "Taxonomy"
					@taxonomy_allocations = project.taxonomies.where('allocated_to_id' => @current_user.id) if project.taxonomies.present?
					if @taxonomy_allocations.present?
						taxonomy_count = @taxonomy_allocations.count
						taxonomy_completed = @taxonomy_allocations.where(:status.in => completed_values).count
						projectwise_total_skus_arr << taxonomy_count
						projectwise_completed_skus_arr << taxonomy_completed
						projectwise_remaining_skus_arr << taxonomy_count - taxonomy_completed

						data[content_process.name] = { others: {completed: taxonomy_completed, remaining: @taxonomy_allocations.not_in('status' => completed_values).count }}

						filewise_data = @taxonomy_allocations.group_by{|i| i.project_file if i.project_file.present? } if @taxonomy_allocations.present? && @taxonomy_allocations != []
						filewise_data.map do |project_file, alloc_data|
						    if project_file.present?
							taxonomy_ids = project_file.present? && project_file.taxonomies.present? ? project_file.taxonomies.pluck(:id) : []
							@filewise_taxonomy_allocations = @taxonomy_allocations.where(:id.in => taxonomy_ids)
							@statuswise_taxonomy_allocations = @filewise_taxonomy_allocations.group_by(&:status) if @filewise_taxonomy_allocations.present?
							statuswise_count = {}
							@statuswise_taxonomy_allocations.map do |status, data| 
								if status == "" || status == nil
									empty_count = @statuswise_taxonomy_allocations[""].present? ? @statuswise_taxonomy_allocations[""].count : 0
									nil_count = @statuswise_taxonomy_allocations[nil].present? ? @statuswise_taxonomy_allocations[nil].count : 0
									statuswise_count["Yet to start"] = empty_count + nil_count
								else
									statuswise_count[status] = data.count
								end
							end

							filewise_taxonomy_count = @filewise_taxonomy_allocations.count
							filewise_taxonomy_completed = @filewise_taxonomy_allocations.where(:status.in => completed_values).count
							data[content_process.name].to_h.merge!({project_file.file_name => {total_skus: filewise_taxonomy_count, 
								statuswise_count: statuswise_count, completed: filewise_taxonomy_completed, 
								remaining: filewise_taxonomy_count - filewise_taxonomy_completed, 
								deadline: project_file.deadline}}) if filewise_taxonomy_count > 0
						    end
						end
					end
				elsif content_process.name == "Schema"
					@schema_allocations = project.schemas.where('allocated_to_id' => @current_user.id) if project.schemas.present?
					if @schema_allocations.present?
						schema_count = @schema_allocations.count
						schema_completed = @schema_allocations.where(:status.in => completed_values).count
						projectwise_total_skus_arr << schema_count
						projectwise_completed_skus_arr << schema_completed
						projectwise_remaining_skus_arr << schema_count - schema_completed

						data[content_process.name] = { others: {completed: schema_completed, 
							remaining: @schema_allocations.not_in('status' => completed_values).count }}

						filewise_data = @schema_allocations.group_by{|i| i.project_file } if @schema_allocations.present? && @schema_allocations != []
						filewise_data.map do |project_file, alloc_data|
						    if project_file.present?
							schema_ids = project_file.present? && project_file.schemas.present? ? project_file.schemas.pluck(:id) : []
							@filewise_schema_allocations = @schema_allocations.where(:id.in => schema_ids) if @schema_allocations.present?
							@statuswise_schema_allocations = @filewise_schema_allocations.group_by(&:status) if @filewise_schema_allocations.present?
							statuswise_count = {}
							@statuswise_schema_allocations.map do |status, data| 
								if status == "" || status == nil
									empty_count = @statuswise_schema_allocations[""].present? ? @statuswise_schema_allocations[""].count : 0
									nil_count = @statuswise_schema_allocations[nil].present? ? @statuswise_schema_allocations[nil].count : 0
									statuswise_count["Yet to start"] = empty_count + nil_count
								else
									statuswise_count[status] = data.count
								end
							end
							filewise_schema_count = @filewise_schema_allocations.count
							filewise_schema_completed = @filewise_schema_allocations.where(:status.in => completed_values).count

							data[content_process.name].to_h.merge!({project_file.file_name => {total_skus: filewise_schema_count, 
								statuswise_count: statuswise_count, completed: filewise_schema_completed, 
								remaining: filewise_schema_count - filewise_schema_completed, 
								deadline: project_file.deadline}}) if filewise_schema_count > 0
						   end
						end
					end
				else
					@allocations = project.allocations.where('allocated_to_id' => @current_user.id, 'content_process_id' => content_process.id) if project.allocations.present?
					if @allocations.present?
						allocation_count = @allocations.count
						completed_count = @allocations.where(:status.in => completed_values).count
						projectwise_total_skus_arr << allocation_count
						projectwise_completed_skus_arr << completed_count
						projectwise_remaining_skus_arr << allocation_count - completed_count

						data[content_process.name] = { others: {completed: completed_count, remaining: @allocations.not_in('status' => completed_values).count }}

						filewise_data = @allocations.group_by{|i| i.sku.project_file if i.sku.present?} if @allocations.present? && @allocations != []
						filewise_data.map do |project_file, alloc_data|
						    if project_file.present?
							sku_ids = project_file.skus.present? ? project_file.skus.pluck(:id) : []
							@filewise_allocations = @allocations.where(:sku_id.in => sku_ids) if @allocations.present?

							@statuswise_allocations = @filewise_allocations.group_by(&:status) if @filewise_allocations.present?
							statuswise_count = {}
							@statuswise_allocations.map do |status, data|
							    if	status == nil || status == "" 
								  empty_count = @statuswise_allocations[""].present? ? @statuswise_allocations[""].count : 0
								  nil_count = @statuswise_allocations[nil].present? ? @statuswise_allocations[nil].count : 0
								  statuswise_count["Yet to start"] = empty_count + nil_count 
						        else
								  statuswise_count[status] = data.count
							    end
							end
							filewise_allocation_count = @filewise_allocations.count
							filewise_completed_count = @filewise_allocations.where(:status.in => completed_values).count

							data[content_process.name].to_h.merge!({project_file.file_name => {total_skus: filewise_allocation_count, 
								statuswise_count: statuswise_count, 
								completed: filewise_completed_count, 
								remaining: filewise_allocation_count - filewise_completed_count, 
								deadline: project_file.deadline}}) if filewise_allocation_count > 0
						    end
						end
					end
				   @allocations = nil
				end				
			end
			data["others"] = { completed: projectwise_completed_skus_arr.sum, remaining: projectwise_remaining_skus_arr.sum}
			data["project_code"] = project.project_code
			project_details[project.name] = data if projectwise_total_skus_arr.sum > 0
			total_skus_arr << projectwise_total_skus_arr.sum
			completed_skus_arr << projectwise_completed_skus_arr.sum
			remaining_skus_arr << projectwise_remaining_skus_arr.sum
		end

		response_data = {user_details: project_details, others: { total_skus: total_skus_arr.sum, completed: completed_skus_arr.sum, remaining: remaining_skus_arr.sum}}
		return response_data		
	end

end

module UserProductionLog
  extend ActiveSupport::Concern

  def update_production_info(user_id, project_name, production_ids, content_process_name)
    @user_log = UserLog.find_by(user_id: user_id, log_date: Date.today) rescue nil

    begin 
      if @user_log.present?
      	  updatable_production_info = @user_log.production_info
      	  existing_production_ids = updatable_production_info[project_name][content_process_name] if updatable_production_info.present? && updatable_production_info[project_name].present?
      	  latest_production_ids = existing_production_ids.present? ? existing_production_ids + production_ids : production_ids
      	  if updatable_production_info.present? 
      	  	if updatable_production_info[project_name].present? 
      	  	   updatable_production_info[project_name][content_process_name] = latest_production_ids.uniq 
      	  	else
      	  	   updatable_production_info[project_name] = {content_process_name => latest_production_ids}
      	  	end
      	  else      	  	
      	  	 updatable_production_info = {project_name => {content_process_name => latest_production_ids}}
      	  end
      	  #production_count = updatable_production_info.values.flatten.count if updatable_production_info.present?
      	  production_arr = updatable_production_info.map{|project, prod_info| prod_info.values.flatten.uniq.count}
          @user_log.update(production_count: production_arr.sum, production_info: updatable_production_info)
      else
      	  production_count = production_ids.uniq.count if production_ids.present?
          @user_log = UserLog.create(user_id: user_id, log_date: Date.today, production_count: production_count, production_info: {project_name => {content_process_name => production_ids.uniq}})
      end
    rescue => error
      return error
    end
  end

end

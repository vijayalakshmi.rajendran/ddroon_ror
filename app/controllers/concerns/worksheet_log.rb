module WorksheetLog
  extend ActiveSupport::Concern

  def create_worksheet_log(project_id, file_id, content_process_id, user_id, sheet_id)
      @user_worksheet_log = UserWorksheetLog.create(project_id: project_id, project_file_id: file_id, content_process_id: content_process_id, user_id: user_id, sheet_id: sheet_id)
      NotificationMailer.delay(run_at: 24.hours.from_now).sheet_not_saved_alert(@user_worksheet_log.id)
  end

  def delete_worksheet_log(project_id, file_id, content_process_id, user_id, sheet_id)
      sheet_log = UserWorksheetLog.find_by(project_id: project_id, project_file_id: file_id, content_process_id: content_process_id, user_id: user_id, sheet_id: sheet_id) rescue nil
      sheet_log.delete if sheet_log.present?
  end  

end

require 'csv'
class DownloadSheetsController < ApplicationController

	# GET - /download_sku_sheet
	def download_sku_sheet
    DownloadSkuWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
    render json: {status: true, message: "Download is in progress. You will be notified of the status via E-mail shortly."}		
	end

  # GET - /download_taxonomy_sheet
  def download_taxonomy_sheet
    DownloadTaxonomyWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
    render json: {status: true, message: "Download is in progress. You will be notified of the status via E-mail shortly."}    
  end

  # GET - /download_schema_sheet
  def download_schema_sheet
    DownloadSchemaWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
    render json: {status: true, message: "Download is in progress. You will be notified of the status via E-mail shortly."}    
  end

end

















# ------------------------------------------------------------------------------------------------------------------------------------

# # GET - /download_sku_sheet
#   def download_sku_sheet
#     DownloadSkuWorker.perform_async(params[:db_name], params[:file_id], @current_user.id)
#     render json: {status: true, message: "Download is in progress. You will be notified of the status via email shortly."}
#     # @project = Project.find_by(db_name: params[:db_name])
#     # if @project.present?      
#     #   @project.tenancy!

#     #   project_file = ProjectFile.find(params[:file_id]) if params[:file_id].present?
#     #   sheet_headers = ["id"] + project_file.actual_headers
#     #   allocation_headers = {}
#     #   project_file.file_process_mappings.map do |fp_mapping|
#    #         process_allocated_to = "#{fp_mapping.content_process.name}_by" if fp_mapping.content_process.present?
#     #      sheet_headers << process_allocated_to unless sheet_headers.include?(process_allocated_to)
#     #      allocation_headers[process_allocated_to] = fp_mapping.content_process_id
#     #   end

#     #   @skus = Sku.where(project_file_id: params[:file_id])

#     #   # result = CSV.generate do |csv|
#     #   #    csv << sheet_headers       
#     #   #   @skus.map do |sku|
#   #  #                  sku_data = []
#   #  #                  sheet_headers.map do |i|
#   #  #                      if allocation_headers.keys.include?(i)
#   #  #                          process_allocation = sku.allocations.find_by(content_process_id: allocation_headers[i]) rescue nil if sku.allocations.present?
#   #  #                          content_producer_name = process_allocation.present? && process_allocation.allocated_to.present? ? process_allocation.allocated_to.username : ""
#   #  #                          sku_data << content_producer_name
#   #  #                      else
#   #  #                          sku_data <<  sku[i]
#   #  #                      end
#   #  #                  end
#   #  #                  csv << sku_data
#   #  #              end
#   #  #        end 

#   #       document_name = project_file.file_name.split(".")[0]

#   #     CSV.open("#{Rails.root}/public/#{document_name}.csv", "wb") do |csv|
#     #       csv << sheet_headers        
#     #     @skus.map do |sku|
#   #                   sku_data = []
#   #                   sheet_headers.map do |i|
#   #                       if allocation_headers.keys.include?(i)
#   #                           process_allocation = sku.allocations.find_by(content_process_id: allocation_headers[i]) rescue nil if sku.allocations.present?
#   #                           content_producer_name = process_allocation.present? && process_allocation.allocated_to.present? ? process_allocation.allocated_to.username : ""
#   #                           sku_data << content_producer_name
#   #                       else
#   #                           sku_data <<  sku[i]
#   #                       end
#   #                   end
#   #                   csv << sku_data
#   #               end
#     #   end

#     #   DownloadsMailer.send_generated_file(@current_user.email_id, @project.name, document_name).deliver!

#   #     # send_data result, :type => 'text/csv; charset=utf-8; header=present', :disposition => 'attachment; filename=payments.csv' 
#     # else
#     #   render json: {status: false, message: "Invalid Project Code"}
#     # end
#   end










class PullTaxonomyWorksheetsController < ApplicationController
    
	include WorksheetLog # To include the worksheet_log concern

	# POST - /pull_taxonomy_worksheets
	def pull_taxonomy_data

		@project = Project.find_by(db_name: params[:db_name])

		if @project.present?
			
			@project.tenancy!

			project_file = ProjectFile.find_by(file_name: params[:file_name]) rescue nil if params[:file_name].present?
			content_process = ContentProcess.find_by(name: params[:content_process_name]) rescue nil if params[:content_process_name].present?

			@user_worksheet_log = UserWorksheetLog.find_by(project_id: @project.id, project_file_id: project_file.id, content_process_id: content_process.id, user_id: params[:user_id]) rescue nil

			if @user_worksheet_log.present?
				# Authenticate a session with your Service Account
				session = GoogleDrive::Session.from_service_account_key("client_secret.json")
				# Get the first worksheet
				spreadsheet = session.spreadsheet_by_key(@user_worksheet_log.sheet_id)
				worksheet = spreadsheet.worksheets.first
				sheet_data = worksheet.rows
				column_names = sheet_data[0]
				# new_header_values = project_file.actual_headers - (column_names - ["id"])
				# total_header_values = project_file.actual_headers + new_header_values
				new_header_values = column_values - ["id"] if column_names.present?
				project_file.update(actual_headers: new_header_values) if new_header_values.present?

				sheet_data[1..-1].map do |data|
					updatable_data = {}
					column_names.each_with_index do |col_name, index| 
						unless project_file.input_headers.include?(col_name)
							updatable_data[col_name] = data[index]
						end
					end
					@taxonomy = Taxonomy.find(data[column_names.index("id")]) if column_names.index("id").present? && data[column_names.index("id")].present?
					if @taxonomy.present?
						@taxonomy.update(updatable_data)
					end
				end
				# To delete the content producer worksheet activity when the activity gets completed
				delete_worksheet_log(@project.id, project_file.id, content_process.id, params[:user_id], @user_worksheet_log.sheet_id)
				render json: {status: true, message: "Data saved Successfully"}
			else
				render json: {status: false, message: "No WorkLog Found"}
			end
		else
			render json: {status: false, message: "Invalid Project Code"}
		end
		
	end


end

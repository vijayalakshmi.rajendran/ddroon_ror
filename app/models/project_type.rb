#require 'mongoid/paranoia'
class ProjectType
#acts_as_paranoid
  include Mongoid::Document
 # include Mongoid::Paranoia
  include Mongoid::Timestamps
 
  field :type_name, type: String
  # field :created_by, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime

  has_many :projects
  belongs_to :created_by, class_name: "User", foreign_key: :created_by_id

  validates_presence_of :type_name
  validates_uniqueness_of :type_name

end

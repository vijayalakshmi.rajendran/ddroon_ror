require 'securerandom'

class User
#acts_as_paranoid
  include Mongoid::Document
  include Mongoid::Timestamps
 # include Mongoid::Paranoia
  include ActiveModel::SecurePassword
 # include Mongoid::Paranoia
  extend Enumerize

 
  #  acts_as_paranoid

  field :first_name, type: String
  field :last_name, type: String
  field :username, type: String
  field :email_id, type: String
  field :password_digest, type: String
  field :avatar, type: String
  field :status
  field :designation, type: String
  field :date_of_joining, type: Date
  field :otp, type: String
  field :fcm_token, type: String
  # field :created_by, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime  
  
  has_many :user_logs
  has_many :client_types
  has_many :project_types
  has_many :regions
  has_many :content_processes
  has_many :services
  has_many :projects
  # has_many :imported_files, class_name: "ProjectFile"
  belongs_to :role
  has_many :created_users, class_name: 'User'
  belongs_to :created_by, class_name: "User", foreign_key: :created_by_id, optional: true

  has_secure_password

  enumerize :status, in: [:ACTIVE, :INACTIVE], default: :ACTIVE

  validates_presence_of :username, :email_id
  validates_uniqueness_of :username, :email_id


  def self.db_backup
     system("sudo mongodump --db ddroon_prod --username ddroon_live --password ddroon_live --gzip --out ../backup")
  end


   # For identifying the user based on the username given
  def self.identify_user(email_id)
    user = User.find_by(email_id: email_id) if email_id.present?
    otp = rand(36**8).to_s(36)
    user.update!(otp: otp) if user.present?
    if user.present? 
        VerificationMailer.send_verification_code(user.id).deliver_now
        data = {status: true, user_id: user.id, message: "Verification code have been sent to the registered Email Id"}
    else
        data = {status: false, message: "Invalid Email Id"}
    end
    return data
  end

  # For verifying the verification code entered by the user while changing password
  def self.verify_code(user_id, verification_code, password, password_confirmation)
    user = User.find(user_id) if user_id.present?
    if user.present? && user.otp == verification_code 
        user.update!(password: password, password_confirmation: password_confirmation, otp: nil)
        message = "Password changed successfully"
        data = {status: true, message: message}
    else
        data = {status: false, message: "Invalid OTP"}
    end
    return data
  end

  # For changing the password of the user when account is logged in
  def self.reset_password(user_id, old_password, password, password_confirmation)
    user = User.find(user_id) if user_id.present?
    if user.present? && user&.authenticate(old_password) # (params[:old_password])
      user.update!(password: password, password_confirmation: password_confirmation)
      data = {status: true, message: "Password Changed Successfully"}
    else
      data = {status: false, message: "Invalid credentials"}
    end
    return data
  end

  def is_superadmin?
    self.role and self.role.role_name == "SUPERADMIN"
  end

  def is_admin?
    self.role and self.role.role_name == "ADMIN"
  end

  def is_content_producer?
    self.role and self.role.role_name == "CONTENT PRODUCER"
  end

  def is_QA_admin?
    self.role and self.role.role_name == "QA ADMIN"
  end

  def is_QA_user?
    self.role and self.role.role_name == "QA USER"
  end

end

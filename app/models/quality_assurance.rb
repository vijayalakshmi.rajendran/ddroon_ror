class QualityAssurance
  include Mongoid::Document
  include Mongoid::Timestamps

  field :error_percentage, type: String
  field :marked_samples, type: String
  field :audit_headers, type: Array 
  field :audit_columns, type: Object
  field :audit_batch, type: String
  field :status, type: String

  belongs_to :sku
  belongs_to :project_file
  belongs_to :content_process
  belongs_to :allocated_to, class_name: "User", foreign_key: :allocated_to_id
  belongs_to :allocated_by, class_name: "User", foreign_key: :allocated_by_id

  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
end

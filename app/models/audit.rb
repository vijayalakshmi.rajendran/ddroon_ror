class Audit
  include Mongoid::Document
  include Mongoid::Timestamps
  extend Enumerize

  field :project_code, type: String
  field :level
  field :created_at, type: DateTime
  field :updated_at, type: DateTime

  belongs_to :project_file

  enumerize :level, in: [:FA, :SA, :TA, :OA, :IQA], default: :FA

  def project
	  	Project.find_by_project_code(self.project_code)
	end

end

class UserWorksheetLog

#acts_as_paranoid
  include Mongoid::Document
  include Mongoid::Timestamps
#  include Mongoid::Paranoia  
  #  acts_as_paranoid

  belongs_to :project
  field :project_file_id, type: String
  belongs_to :content_process
  belongs_to :user
  field :sheet_id, type: String
  field :saved?, type: Boolean, default: false
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
end

#require 'mongoid/paranoia'
class ProjectFile

#acts_as_paranoid
  include Mongoid::Document
#  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Tenant
   
  #  acts_as_paranoid

  field :file_name, type: String
  field :file_type, type: String
  field :file_size, type: Integer
  field :sku_count, type: Integer
  field :deadline, type: Date
  # field :imported_by, type: String
  field :input_headers, type: Array, default: []
  field :actual_headers, type: Array
  field :mapped_headers, type: Object
  field :upload_in_progress, type: Boolean, default: false
  field :is_latest_version, type: Boolean, default: true
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
  field :is_audit_level, type: String
  field :audit_stage, type: String
  field :file_key, type: String
  # has_and_belongs_to_many :content_processes
  has_many :file_process_mappings, dependent: :destroy

  has_many :skus, dependent: :destroy
  has_many :allocations, dependent: :destroy
  has_many :taxonomies, dependent: :destroy
  has_many :schemas, dependent: :destroy
  belongs_to :imported_by, class_name: "User", foreign_key: :imported_by_id
  has_many :audits, dependent: :destroy
  has_many :quality_assurances, dependent: :destroy

  accepts_nested_attributes_for :skus, :file_process_mappings, :taxonomies, :schemas

  validates_presence_of :file_name

  validates_uniqueness_of :file_name

end

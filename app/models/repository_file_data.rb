#require 'mongoid/paranoia'
class RepositoryFileData
  include Mongoid::Document
 # include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Attributes::Dynamic
  include Mongoid::Search

  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime  

  belongs_to :repository_file
  index({ repository_file_id: 1 }, { unique: true})  

  search_in :search_data

  def search_data
    exceptional_keys = ['_id', 'repository_file_id', '_keywords', 'created_at', 'updated_at']
    self.attributes.select{|k,v| v.is_a?(String) unless exceptional_keys.include?(k)}.values.join(' ')
  end
  
end

require 'mongoid/paranoia'
class Project

  include Mongoid::Document
  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Tenancy
  include Mongoid::Search
 # include Mongoid::Paranoia
  extend Enumerize

  tenant_key :db_name

  # acts_as_paranoid

  field :name, type: String
  field :db_name, type: String
  field :project_code, type: String
  field :client_logo, type: String
  field :deadline, type: Date
  field :status
  # field :created_by, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime

  belongs_to :project_type
  belongs_to :client_type
  belongs_to :region
  belongs_to :service
  belongs_to :created_by, class_name: "User", foreign_key: :created_by_id
  
  # has_and_belongs_to_many :content_processes
  has_many :project_process_mappings

  enumerize :status, in: [:ACTIVE, :INACTIVE, :COMPLETED]

  has_tenant :project_files
  has_tenant :skus
  has_tenant :taxonomies
  has_tenant :schemas
  has_tenant :allocations
  has_many :user_worksheet_logs, dependent: :destroy

  accepts_nested_attributes_for :project_process_mappings

  search_in :name, :project_code

  validates_presence_of :name, :project_code

  validates_uniqueness_of :name, :project_code, :db_name

  #validates :title, uniqueness: { conditions: -> { where(deleted_at: nil) } }

  def self.projects
    Project.where(project_code: {'$in': Audit.all.collect(&:project_code)})
    #Project.where(:project_code => Audit.all.collect(&:project_code))
  end
  
end

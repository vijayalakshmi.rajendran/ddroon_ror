class FileProcessMapping
#acts_as_paranoid
  include Mongoid::Document
 # include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Tenant
  extend Enumerize

  # acts_as_paranoid

  field :deadline, type: Date
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
   
  belongs_to :project_file
  belongs_to :content_process

  field :type_of_file
  
  enumerize :type_of_file, in: [:EXISTING, :NEW], default: :NEW

end

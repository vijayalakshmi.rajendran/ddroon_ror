class Service
#  acts_as_paranoid
  include Mongoid::Document
  include Mongoid::Timestamps
  #include Mongoid::Paranoia
  # acts_as_paranoid
  
  field :name, type: String
  # field :created_by, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime 

  has_and_belongs_to_many :content_processes
  has_and_belongs_to_many :services

  has_many :projects
  belongs_to :created_by, class_name: "User", foreign_key: :created_by_id

  validates_presence_of :name
  validates_uniqueness_of :name

end

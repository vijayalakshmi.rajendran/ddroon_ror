class Role
#acts_as_paranoid
  include Mongoid::Document
  include Mongoid::Timestamps
  #include Mongoid::Paranoia    
  #  acts_as_paranoid

  field :role_name, type: String
  field :level, type: Integer
  field :role_permissions, type: Object
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime   
  
  has_many :users

  validates_presence_of :role_name, :level
  validates_uniqueness_of :role_name

end

class UserLog
#  acts_as_paranoid
  include Mongoid::Document
  include Mongoid::Timestamps
#  include Mongoid::Paranoia
  # acts_as_paranoid
  belongs_to :user
  field :log_date, type: Date  
  field :login_at, type: Array
  field :log_out_at, type: Array
  field :production_count, type: Integer
  field :production_info, type: Object  

end

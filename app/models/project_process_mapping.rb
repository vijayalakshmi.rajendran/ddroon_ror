class ProjectProcessMapping
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Paranoia
  # acts_as_paranoid

  field :deadline, type: Date
  field :create_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
  belongs_to :project
  belongs_to :content_process
end

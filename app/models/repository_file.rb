class RepositoryFile
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Search
  extend Enumerize
  
  field :name, type: String
  field :file_type, type: String
  field :file_size, type: Integer
  field :data_count, type: Integer
  field :file_category
  enumerize :file_category, in: ["Taxonomy", "Schema", "UOM Style Guide", "Specification Document", "Reference PDF", "LOV Values"]
  field :s3_url, type: String
  belongs_to :project, optional: true
  field :project_name, type: String
  field :file_headers, type: Array
  field :upload_in_progress, type: Boolean, default: false
  belongs_to :imported_by, class_name: "User", foreign_key: :imported_by_id  
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime

  search_in :name

  has_many :repository_file_datas, dependent: :destroy
  accepts_nested_attributes_for :repository_file_datas

end

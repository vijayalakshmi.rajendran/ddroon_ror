# require 'mongoid/paranoia'
class Allocation

  include Mongoid::Document
#  include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Tenant
  # field :allocated_to, type: String
  # field :allocated_by, type: String
  field :status, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime  

  belongs_to :content_process
  belongs_to :project_file, optional: true
  belongs_to :sku
  belongs_to :allocated_to, class_name: "User", foreign_key: :allocated_to_id
  belongs_to :allocated_by, class_name: "User", foreign_key: :allocated_by_id

  index({ sku_id: 1, content_process_id: 1 }, { unique: true})

  validates_uniqueness_of :sku_id, scope: :content_process_id

end

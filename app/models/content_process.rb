class ContentProcess
 # acts_as_paranoid
  include Mongoid::Document
  #include Mongoid::Paranoia
  include Mongoid::Timestamps
 
  # acts_as_paranoid

  field :name, type: String
  field :process_columns, type: Array
  field :collection_, type: String
  field :status_values, type: Object
  # field :created_by, type: String
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime  

  has_and_belongs_to_many :services
  # has_and_belongs_to_many :projects
  has_many :project_process_mappings
  # has_and_belongs_to_many :project_files
  has_many :file_process_mappings
  has_many :allocations
  has_many :user_worksheet_logs, dependent: :destroy
  has_many :quality_assurances

  belongs_to :created_by, class_name: "User", foreign_key: :created_by_id, optional: true

  validates_presence_of :name
  validates_uniqueness_of :name

end

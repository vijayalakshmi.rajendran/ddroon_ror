#require 'mongoid/paranoia'
class Sku
  include Mongoid::Document
 # include Mongoid::Paranoia
  include Mongoid::Timestamps
  include Mongoid::Tenant
  include Mongoid::Attributes::Dynamic
  include Mongoid::Search
  extend Enumerize

  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime  
  field :is_audit
  enumerize :is_audit, in: [:YES, :NO], default: :NO

  belongs_to :project_file
  index({ project_file_id: 1 }, { unique: true})

  has_many :allocations, dependent: :destroy
  has_many :quality_assurances, dependent: :destroy
  
  accepts_nested_attributes_for :allocations

  search_in :search_data

  def search_data
    # concatenate all String fields' values
    exceptional_keys = ['_id', 'project_file_id', '_keywords', 'created_at', 'updated_at']
    self.attributes.select{|k,v| v.is_a?(String) unless exceptional_keys.include?(k)}.values.join(' ')
  end
  
end

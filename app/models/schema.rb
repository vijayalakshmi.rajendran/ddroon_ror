#require 'mongoid/paranoia'
class Schema
  include Mongoid::Document
  include Mongoid::Timestamps
#  include Mongoid::Paranoia
  include Mongoid::Tenant
  include Mongoid::Attributes::Dynamic
  include Mongoid::Search
  extend Enumerize

  field :status, type: String  
  field :created_at, type: DateTime
  field :updated_at, type: DateTime
  field :deleted_at, type: DateTime
  field :is_audit
  enumerize :is_audit, in: [:YES, :NO], default: :NO
  
  belongs_to :project_file
  belongs_to :allocated_to, class_name: "User", foreign_key: :allocated_to_id, optional: true
  belongs_to :allocated_by, class_name: "User", foreign_key: :allocated_by_id, optional: true

  search_in :search_data

  def search_data
    # concatenate all String fields' values
    exceptional_keys = ['_id', 'project_file_id', '_keywords', 'created_at', 'updated_at', 'allocated_to_id', 'allocated_by_id']
    self.attributes.select{|k,v| v.is_a?(String) unless exceptional_keys.include?(k)}.values.join(' ')
  end

end

require 'sidekiq'
require 'csv'
# require 'byebug'

class DownloadSkuWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(db_name, file_id, current_user_id)
      begin
        @project = Project.find_by(db_name: db_name) rescue nil
        @user = User.find(current_user_id) rescue nil
        if @project.present?      
          @project.tenancy!
          project_file = ProjectFile.find(file_id) if file_id.present?
          sheet_headers = ["id"] + project_file.actual_headers
          allocation_headers = {}
          project_file.file_process_mappings.map do |fp_mapping|
               process_allocated_to = "#{fp_mapping.content_process.name}_by" if fp_mapping.content_process.present?
             sheet_headers << process_allocated_to unless sheet_headers.include?(process_allocated_to)
             allocation_headers[process_allocated_to] = fp_mapping.content_process_id
          end
          @skus = Sku.where(project_file_id: file_id)          
          document_name = project_file.file_name

          # CSV.open("#{Rails.root}/public/#{document_name}.csv", "wb") do |csv|
          #   csv << sheet_headers        
          #   @skus.map do |sku|
          #     sku_data = []
          #     sheet_headers.map do |i|
          #       if allocation_headers.keys.include?(i)
          #         process_allocation = sku.allocations.find_by(content_process_id: allocation_headers[i]) rescue nil if sku.allocations.present?
          #         content_producer_name = process_allocation.present? && process_allocation.allocated_to.present? ? process_allocation.allocated_to.username : ""
          #         sku_data << content_producer_name
          #       else
          #         sku_data <<  sku[i]
          #       end
          #     end
          #     csv << sku_data
          #   end
          # end

          result = Axlsx::Package.new do |obj|
            obj.workbook.add_worksheet(name: "Sheet Name") do |sheet|
              sheet.add_row sheet_headers
              @skus.map do |sku|
                sku_data = []
                sheet_headers.map do |i|
                  if allocation_headers.keys.include?(i)
                    process_allocation = sku.allocations.find_by(content_process_id: allocation_headers[i]) rescue nil if sku.allocations.present?
                    content_producer_name = process_allocation.present? && process_allocation.allocated_to.present? ? process_allocation.allocated_to.username : ""
                    sku_data << content_producer_name
                  else
                    sku_data <<  sku[i]
                  end
                end                
                sheet.add_row sku_data
              end              
            end
          end
          result.serialize("#{Rails.root}/public/#{document_name}")

          DownloadsMailer.send_generated_file(@user.email_id, @project.name, document_name).deliver!
	        # File.delete("#{Rails.root}/public/#{document_name}.csv") if File.exist?("#{Rails.root}/public/#{document_name}.csv")
          File.delete("#{Rails.root}/public/#{document_name}") if File.exist?("#{Rails.root}/public/#{document_name}")
        end
      rescue => error 
          DownloadsMailer.notify_download_failure({error_log: error}, @user.email_id, @project.name, document_name).deliver!
      end
  end

end

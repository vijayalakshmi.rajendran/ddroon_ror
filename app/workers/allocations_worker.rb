# require 'sidekiq'
# require 'logger'

class AllocationsWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(file_id, column_name, content_process_id)
  	  project_file = ProjectFile.find(file_id)
      @skus = Sku.where(project_file_id: project_file.id)
      sku_ids = @skus.pluck(:_id)
      column_values = @skus.pluck(column_name).uniq
      allocation_data = {}
      column_values.map do |i|
        column_sku_ids = @skus.where(column_name => i).pluck(:id) if @skus.where(column_name => i).present?
        allocated_skus = Allocation.where(:sku_id.in => column_sku_ids, :content_process_id => content_process_id)
        allocated_skus = allocated_skus.not(allocated_to_id: nil) if allocated_skus.present?
	      allocated_sku_data = allocated_skus.map{|i| {sku_id: i.sku_id, allocated_to: i.allocated_to}}
        allocated_sku_ids = allocated_skus.present? ? allocated_skus.pluck(:sku_id) : []
        unallocated_skus = column_sku_ids - allocated_sku_ids
        user_data = {}
        user_skus = allocated_skus.group_by{|i| i.allocated_to} if allocated_skus.present?
        user_skus.map{|k, v| user_data[k.username] = v.map{|i| i.sku_id} if k.present?} if user_skus.present?
        allocation_data[i] = {allocated_skus: allocated_sku_data, unallocated_skus: unallocated_skus, user_data: user_data}
      end
      total_allocated_count = Allocation.where(:sku_id.in => sku_ids, :content_process_id => content_process_id).count
      return {allocation: allocation_data, allocated: total_allocated_count, unallocated: @skus.count - total_allocated_count}
  end

end

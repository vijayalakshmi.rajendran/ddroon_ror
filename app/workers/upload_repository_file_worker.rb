require 'sidekiq'
require 'logger'
require 'enumerator'
# require 'byebug'

class UploadRepositoryFileWorker
  require 'roo'
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(repository_file_id, sheet_data, current_user_email_id)    
    begin
       @repository_file = RepositoryFile.find(repository_file_id["$oid"]) rescue nil
        slice_limit = sheet_data.count / 2
    	sheet_data.each_slice(slice_limit) do |data|
            @repository_file.update(repository_file_datas_attributes: data) if @repository_file.present?
        end               
        @repository_file.update(upload_in_progress: false) if @repository_file.present?        
  	RepositoryMailer.notify_upload_success(@repository_file.project_name, @repository_file.name, current_user_email_id).deliver!
    rescue => error 
        RepositoryMailer.notify_upload_failure({error_log: error}, @repository_file.project_name, @repository_file.name, current_user_email_id).deliver!
    end
  end

end

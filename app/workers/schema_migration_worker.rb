require 'sidekiq'
require 'csv'
# require 'byebug'

class SchemaMigrationWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(db_name, file_id, current_user_id)   
      begin
        @project = Project.find_by(db_name: db_name) rescue nil
        @user = User.find(current_user_id) rescue nil
        if @project.present?      
          @project.tenancy!
          project_file = ProjectFile.find(file_id) if file_id.present?
          sheet_headers = project_file.actual_headers if project_file.present?
          sheet_headers << "allocated_to" unless sheet_headers.include?("allocated_to")          
          
          @schemas = Schema.where(project_file_id: file_id)         
          document_name = project_file.file_name  # .split(".")[0] + ".csv"                    
          file_type = project_file.file_type

          @repository_file = RepositoryFile.find_by(project_name: @project.name, name: document_name) rescue nil

          file_data = []

          unless @repository_file.present?
            # CSV.open("#{Rails.root}/public/#{document_name}", "wb") do |csv|
            #   csv << sheet_headers        
            #   @schemas.map do |schema|      
            #     schema_data = []
		          #   schema_json = {}
            #     sheet_headers.map do |i|
            #       if i == "allocated_to"
            #         user = User.find(schema.allocated_to_id) rescue nil
            #         username = user.present? ? user.username : ""
            #         schema_data << username
		          #       schema_json["allocated_to"] = username
            #       elsif i == "allocated_by"
            #         user = User.find(schema.allocated_by_id) rescue nil
            #         username = user.present? ? user.username : ""
            #         schema_data << username
		          #       schema_json["allocated_to"] = username
            #       else
            #         schema_data << schema[i]
		          #       schema_json[i] = schema[i]
            #       end
            #     end
            #     csv << schema_data             
            #     file_data << schema_json
            #   end
            # end

            result = Axlsx::Package.new do |obj|
              obj.workbook.add_worksheet(name: "Sheet Name") do |sheet|
                sheet.add_row sheet_headers
                @schemas.map do |schema|      
                  schema_data = []
                  schema_json = {}
                  sheet_headers.map do |i|
                    if i == "allocated_to"
                      user = User.find(schema.allocated_to_id) rescue nil
                      username = user.present? ? user.username : ""
                      schema_data << username
                      schema_json["allocated_to"] = username
                    elsif i == "allocated_by"
                      user = User.find(schema.allocated_by_id) rescue nil
                      username = user.present? ? user.username : ""
                      schema_data << username
                      schema_json["allocated_to"] = username
                    else
                      schema_data << schema[i]
                      schema_json[i] = schema[i]
                    end
                  end
                  sheet.add_row schema_data
                  file_data << schema_json
                end                
              end
            end
            result.serialize("#{Rails.root}/public/#{document_name}")

            @file_path = "#{Rails.root}/public/#{document_name}"
            file_size = File.size("#{Rails.root}/public/#{document_name}")

            @s3 = Aws::S3::Resource.new
            bucket = @s3.bucket("ddroon")
            obj = bucket.object(document_name)
            obj.upload_file(@file_path, content_type: file_type, acl: 'public-read')
            url = obj.public_url

            @repository_file = RepositoryFile.new(name: document_name, file_type: file_type, file_size: file_size, file_category: "Schema", s3_url: url, project_id: @project.id, project_name: @project.name, imported_by_id: @user.id, file_headers: sheet_headers, upload_in_progress: false, repository_file_datas_attributes: file_data)

            if @repository_file.save
                RepositoryMailer.notify_migration_success(@user.email_id, @project.name, document_name).deliver!
                project_file.delete
            else
              RepositoryMailer.notify_migration_failure({error_log: "Something went wrong. Please try again"}, @user.email_id, @project.name, document_name).deliver!   
            end            
          else
            RepositoryMailer.notify_migration_failure({error_log: "Filename already exists in Repository"}, @user.email_id, @project.name, document_name).deliver!
          end
          File.delete("#{Rails.root}/public/#{document_name}") if File.exist?("#{Rails.root}/public/#{document_name}")   
        end
      rescue => error 
          RepositoryMailer.notify_migration_failure({error_log: error}, @user.email_id, @project.name, document_name).deliver!
          File.delete("#{Rails.root}/public/#{document_name}") if File.exist?("#{Rails.root}/public/#{document_name}")
      end
  end

end

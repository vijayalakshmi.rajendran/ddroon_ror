require 'sidekiq'
require 'logger'
require 'enumerator'
# require 'byebug'

class UploadFileWorker
  require 'roo'
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(project_id, project_file_id, sheet_data, data_for_the_process, current_user_id)
    	  # NotificationMailer.notify_upload_failure("Entered Sidekiq worker").deliver!
    begin
        # NotificationMailer.notify_upload_failure("Entered Exception handler- #{project_file_id["$oid"]}").deliver!
        @project = Project.find(project_id) rescue nil
  	    @project.tenancy! if @project.present?
  	    @project_file = ProjectFile.find(project_file_id["$oid"]) rescue nil
  	    file_status = @project_file.present? ? true : false
        # slice_limit = sheet_data.count / 2
     	  if data_for_the_process == "Sku"
          sheet_data.each_slice(1000) do |data|
            @project_file.update(skus_attributes: data) if @project_file.present?
          end     
        elsif data_for_the_process == "Taxonomy"
          sheet_data.each_slice(1000) do |data|
            @project_file.update(taxonomies_attributes: data) if @project_file.present?
          end     
        elsif data_for_the_process == "Schema"
          sheet_data.each_slice(1000) do |data|
            @project_file.update(schemas_attributes: data) if @project_file.present?
          end     
        end        
        @project_file.update(upload_in_progress: false) if @project_file.present?
  	    NotificationMailer.notify_upload_success(@project.name, @project_file.file_name, current_user_id).deliver!
    rescue => error 
        NotificationMailer.notify_upload_failure({error_log: error}, @project.name, @project_file.file_name, current_user_id).deliver!
    end
  end

end

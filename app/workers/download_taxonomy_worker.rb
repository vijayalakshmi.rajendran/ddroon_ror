require 'sidekiq'
require 'csv'
# require 'byebug'

class DownloadTaxonomyWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(db_name, file_id, current_user_id)
      begin
        @project = Project.find_by(db_name: db_name) rescue nil
        @user = User.find(current_user_id) rescue nil
        if @project.present?      
          @project.tenancy!
          project_file = ProjectFile.find(file_id) if file_id.present?
          sheet_headers = ["id"] + project_file.actual_headers
          sheet_headers << "allocated_to" unless sheet_headers.include?("allocated_to")
                  
          @taxonomies = Taxonomy.where(project_file_id: file_id)     
          document_name = project_file.file_name #.split(".")[0]

          # CSV.open("#{Rails.root}/public/#{document_name}.csv", "wb") do |csv|
          #   csv << sheet_headers        
          #   @taxonomies.map do |taxonomy|       
          #       taxonomy_data = []
          #       sheet_headers.map do |i| 
          #           if i == "allocated_to"
          #             user = User.find(taxonomy.allocated_to_id) rescue nil
          #             username = user.present? ? user.username : ""
          #             taxonomy_data << username
          #           elsif i == "allocated_by"
          #             user = User.find(taxonomy.allocated_by_id) rescue nil
          #             username = user.present? ? user.username : ""
          #             taxonomy_data << username
          #          else
          #             taxonomy_data << taxonomy[i]
          #          end
          #       end
          #       csv << taxonomy_data
          #   end
          # end

          result = Axlsx::Package.new do |obj|
            obj.workbook.add_worksheet(name: "Sheet Name") do |sheet|
              sheet.add_row sheet_headers
              @taxonomies.map do |taxonomy|       
                taxonomy_data = []
                sheet_headers.map do |i| 
                    if i == "allocated_to"
                      user = User.find(taxonomy.allocated_to_id) rescue nil
                      username = user.present? ? user.username : ""
                      taxonomy_data << username
                    elsif i == "allocated_by"
                      user = User.find(taxonomy.allocated_by_id) rescue nil
                      username = user.present? ? user.username : ""
                      taxonomy_data << username
                   else
                      taxonomy_data << taxonomy[i]
                   end
                end
                sheet.add_row taxonomy_data
              end              
            end
          end
          result.serialize("#{Rails.root}/public/#{document_name}")

          DownloadsMailer.send_generated_file(@user.email_id, @project.name, document_name).deliver!
	        File.delete("#{Rails.root}/public/#{document_name}") if File.exist?("#{Rails.root}/public/#{document_name}")
        end
      rescue => error 
          DownloadsMailer.notify_download_failure({error_log: error}, @user.email_id, @project.name, document_name).deliver!
      end
  end

end

class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :username, :email_id, :status, :role_id, :avatar, :designation, :date_of_joining

  has_one :created_by
end

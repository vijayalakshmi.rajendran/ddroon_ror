class RoleSerializer < ActiveModel::Serializer
  attributes :id, :role_name, :level
end

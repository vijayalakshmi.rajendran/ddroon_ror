class AuditSerializer < ActiveModel::Serializer
  attributes :id, :project_code, :level
end

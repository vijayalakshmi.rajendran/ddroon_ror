class RepositoryFileSerializer < ActiveModel::Serializer
  attributes :id, :name, :s3_url, :file_type, :file_size, :file_headers, :project_name, :upload_in_progress, :created_at, :updated_at, :deleted_at

  has_one :imported_by

end

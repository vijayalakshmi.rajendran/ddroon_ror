class RegionSerializer < ActiveModel::Serializer
  attributes :id, :name, :abbreviation
end

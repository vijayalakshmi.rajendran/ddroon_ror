class ServiceSerializer < ActiveModel::Serializer
  attributes :id, :name, :content_processes, :is_deletable

 def is_deletable
    return object.projects.present? ? false : true
  end

end

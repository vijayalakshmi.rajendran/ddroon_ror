class ProjectTypeSerializer < ActiveModel::Serializer
  attributes :id, :type_name, :is_deletable 

def is_deletable
    return object.projects.present? ? false : true
  end

end

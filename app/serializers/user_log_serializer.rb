class UserLogSerializer < ActiveModel::Serializer
  attributes :id, :log_date, :login_at, :log_out_at, :production_count, :production_info

  has_one :user
end

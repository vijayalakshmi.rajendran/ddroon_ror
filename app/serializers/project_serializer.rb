class ProjectSerializer < ActiveModel::Serializer
  attributes :id, :name, :project_code, :client_logo, :deadline, :status, :created_by, :project_type_id, :client_type_id, :region_id, :service_id, :content_processes #, :allocated_skus, :completed_skus

  has_one :project_type
  has_one :client_type
  has_one :region
  has_one :service
  # has_many :content_processes
  # has_many :project_process_mappings

  def content_processes
    return object.project_process_mappings.map{|i| {id: i.id, content_process: i.content_process, deadline: i.deadline}}
  end

  # def allocated_skus
  # 	object.skus.where('allocations.allocated_to_id' => scope._id).count
  # end

  # def completed_skus
  # 	object.skus.where('allocations.allocated_to_id' => scope._id, 'allocations.status' => "COMPLETED").count
  # end

end

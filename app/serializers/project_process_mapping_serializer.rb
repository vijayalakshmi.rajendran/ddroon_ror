class ProjectProcessMappingSerializer < ActiveModel::Serializer
  attributes :id, :deadline

  has_one :project
  has_one :content_process

end

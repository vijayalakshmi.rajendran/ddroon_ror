class ProjectFileSerializer < ActiveModel::Serializer
  attributes :id, :file_name, :file_type, :file_size, :sku_count, :deadline, :imported_by, :input_headers, :actual_headers, :mapped_headers, :upload_in_progress, :is_latest_version, :created_at, :updated_at, :content_processes, :is_audit_level, :audit_stage # :sku_detail,
  
  # has_many :content_processes
  # has_many :file_process_mappings
  has_one :imported_by

  # def sku_detail
  # 	content_process_ids = object.allocations.pluck(:content_process_id)
  # 	data = {}
  # 	content_process_ids.map do |pro_id| 
 	# 	content_process = ContentProcess.find(pro_id) rescue nil
  #    	 if content_process.present?
		# total_skus = object.skus.count
  #      		completed_skus = object.allocations.where(:content_process_id => pro_id, :status => 'COMPLETED').count
	 #        remaining_skus = total_skus - completed_skus
  # 		data[content_process.name] = {total_skus: total_skus, completed_skus: completed_skus, remaining_skus: remaining_skus}
  #         end
  # 	end
  # 	return data
  # end

  def content_processes
    return object.file_process_mappings.map{|i| {id: i.id, content_process: i.content_process, deadline: i.deadline, type_of_file: i.type_of_file}}
  end

end

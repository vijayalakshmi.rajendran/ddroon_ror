class ContentProcessSerializer < ActiveModel::Serializer
  attributes :id, :name, :status_values
end

class AllocationSerializer < ActiveModel::Serializer
  attributes :id, :allocated_to, :allocated_by, :status, :created_at, :updated_at
  has_one :content_process
end

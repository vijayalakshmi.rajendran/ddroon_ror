class QualityAssuranceSerializer < ActiveModel::Serializer
  attributes :id, :error_percentage, :marked_samples, :audit_headers, :audit_columns, :audit_batch, :status
end

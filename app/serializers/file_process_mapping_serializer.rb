class FileProcessMappingSerializer < ActiveModel::Serializer
  attributes :id, :deadline, :type_of_file

  has_one :project_file
  has_one :content_process 
end

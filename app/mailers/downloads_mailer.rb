class DownloadsMailer < ApplicationMailer

  default :from => ENV["from_mail"]

  def send_generated_file(email_id, project_name, document_name)
  	  attachments["#{document_name}"] = File.read("#{Rails.root}/public/#{document_name}")
      mail(:to => email_id, :subject => "DDROON - Download successful", :body => "Hi, Please find the file #{document_name} of the project #{project_name} attached below")
  end

  def notify_download_failure(error_log, email_id, project_name, document_name)
      mail(:to => email_id, :subject => "DDROON - Download failed", :body => "Hi, your attempt to download the file #{document_name} of the project #{project_name} was failed. Developer Log : #{error_log}")
  end

end

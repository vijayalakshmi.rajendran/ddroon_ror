class RepositoryMailer < ApplicationMailer

  default :from => ENV["from_mail"]

  def notify_migration_success(email_id, project_name, document_name)
      mail(:to => email_id, :subject => "DDROON - Migration successful", :body => "Hi, Your attempt to move the file #{document_name} of the project #{project_name} to repository is successful.")
  end

  def notify_migration_failure(error_log, email_id, project_name, document_name)
      mail(:to => email_id, :cc => ENV["cc_mail"], :subject => "DDROON - Migration failed", :body => "Hi, Your attempt to move the file #{document_name} of the project #{project_name} to repository failed. Developer Log : #{error_log}")
  end

  def notify_upload_success(project_name, file_name, email_id)    
    mail( :to => email_id,
      :subject => "DDROON - #{file_name} Upload Success", :body => "Hi, Your attempt to upload the file #{file_name} of the project #{project_name} to repository is successful.")
  end

  def notify_upload_failure(error_log, project_name, file_name, email_id)
    mail( :to => @user.email_id, :cc => ENV["cc_mail"],
	  :subject => "DDROON - #{file_name} Upload Failed", :body => "Hi, Your attempt to upload the file #{file_name} of the project #{project_name} to repository failed. Developer Log : #{error_log}")
  end


end

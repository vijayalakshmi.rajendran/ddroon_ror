class VerificationMailer < ApplicationMailer

  default :from => ENV["from_mail"]

  def send_verification_code(user_id)
    @user = User.find(user_id) if user_id.present?
    mail( :to => @user.email_id,
    :subject => "DDroon - Verification Code")
  end  

end

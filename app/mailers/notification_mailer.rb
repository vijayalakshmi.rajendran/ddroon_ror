class NotificationMailer < ApplicationMailer

  default :from => ENV["from_mail"]

  def welcome_mail(user_id, password)
    @user = User.find(user_id)
    @password = password

    mail( :to => @user.email_id,
    :subject => "Greetings from DDROON - Welcome #{@user.first_name}")
  end

  def sheet_not_saved_alert(user_worksheet_log_id)
  	@user_worksheet_log = UserWorksheetLog.find(user_worksheet_log_id) rescue nil
  	if @user_worksheet_log.present?
  		@user_worksheet_log.project.tenancy! if @user_worksheet_log.project.present?
      @project_file = ProjectFile.find(@user_worksheet_log.project_file_id) rescue nil
  		mail( :to => @user_worksheet_log.user.email_id,
    	:subject => "Work not saved alert from DDROON - Hi #{@user_worksheet_log.user.first_name}")
  	end
  end


  def notify_upload_success(project_name, file_name, current_user_id)    
    @project_name = project_name
    @file_name = file_name
    @user = User.find(current_user_id) rescue nil
    mail( :to => @user.email_id, :subject => "DDROON - #{file_name} Upload Success") if @user.present?
  end


  def notify_upload_failure(log, project_name, file_name, current_user_id)
    @project_name = project_name
    @file_name = file_name
    @error_log = log
    @user = User.find(current_user_id) rescue nil
    mail( :to => @user.email_id, :cc => ENV["cc_mail"],
	  :subject => "DDROON - #{file_name} Upload Failed") if @user.present?
  end


  # NotificationMailer.delay(run_at: 24.hours.from_now).sheet_not_saved_alert(@user_worksheet_log.id)

end

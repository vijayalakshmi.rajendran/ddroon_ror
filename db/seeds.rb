Role.create!([
        {role_name: "SUPERADMIN", level: 1},
        {role_name: "ADMIN", level: 2},
        {role_name: "CONTENT PRODUCER", level: 3}
])

User.create!([
        {first_name: "Altius", last_name: "Technologies", username: "Superadmin", email_id: "superadmin@gmail.com", password: "superadmin", password_confirmation: "superadmin", role_id: Role.find_by(level: 1).id},
        {first_name: "Karthick", last_name: "B", username: "Karthick", email_id: "bkarthick@altiussolution.com", password: "karthick", password_confirmation: "karthick", role_id: Role.find_by(level: 2).id},
#       {first_name: "Vijayalakshmi", last_name: "Rajendran", username: "VijiRaj", email_id: "vijayalakshmi.rajendran@adcltech.com", password: "vijiraj", password_confirmation: "vijiraj", role_id: Role.last.id},
#       {first_name: "Priyadharshini", last_name: "Kathirvel", username: "PriyaKathir", email_id: "priyadharshini.kathirvel@adcltech.com", password: "priyakathir", password_confirmation: "priyakathir", role_id: Role.last.id}
])

#ClientType.create!([
#       {type_name: "Altius-Direct", created_by_id: User.first.id},
#       {type_name: "Altius-eCommerce", created_by_id: User.first.id}
#])

#ProjectType.create!([
#       {type_name: "Regular", created_by_id: User.first.id},
#       {type_name: "Pilot", created_by_id: User.first.id},
#       {type_name: "POC", created_by_id: User.first.id}
#])

ContentProcess.create!([
        {name: "Sourcing", created_by_id: User.first.id},
        {name: "Taxonomy", created_by_id: User.first.id},
        {name: "Classification", created_by_id: User.first.id},
        {name: "Schema", created_by_id: User.first.id},
        {name: "Data Build", created_by_id: User.first.id},
        {name: "Normalization", created_by_id: User.first.id},
        {name: "Description Generation", created_by_id: User.first.id}
])


regions.map do |i|
        Region.create({name: i, created_by_id: User.first.id})
end

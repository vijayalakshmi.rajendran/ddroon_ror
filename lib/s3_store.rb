require 'aws-sdk'

class S3Store

  def initialize file
    @file = file
    @s3 = Aws::S3::Resource.new
    bucket_name = "ddroon".freeze
    @bucket = @s3.bucket(bucket_name)
  end

  def store
    @obj = @bucket.object(filename)
    @obj.put(body: @file.tempfile, content_type: @file.content_type, acl: 'public-read')
    self
  end

  def url
    @obj.public_url.to_s
  end

  private
  
  def filename
    @filename ||= @file.original_filename.gsub(/[^a-zA-Z0-9_\.]/, '_')
  end
end

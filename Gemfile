source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.7.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.2', '>= 6.0.2.2'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
gem 'sidekiq', '~>6.0.0'
#gem 'sidekiq-status'

gem 'caxlsx'
gem 'caxlsx_rails'

# For using Multitenancy in Mongodb
gem 'mongoid-tenant'

# For multithreading 
gem 'parallel'

# For running a job later 
gem 'delayed_job_mongoid'

# for s3 bucket file upload
gem 'aws-sdk'

gem 'fcm'
# Use Active Model has_secure_password
gem 'bcrypt', '~> 3.1.7'

# For Using Mongodb as database
gem 'bson_ext'
gem 'mongoid'

# For Token based authentication
gem 'jwt'

gem 'whenever', require: false

gem 'rubyzip', '>= 1.2.1'
gem 'axlsx', git: 'https://github.com/randym/axlsx.git', ref: 'c8ac844'
gem 'axlsx_rails'

# For soft delete
gem 'mongoid_paranoia', github: 'simi/mongoid-paranoia'

#gem 'mongoid-paranoia', '~> 2.0'
# For using enumeration in Mongodb
gem 'enumerize'

# For using google sheet API
gem 'google_drive'

# For Spreadsheet Import
gem "roo", "~> 2.8.0"

# For Search Implementation
# gem 'mongoid_search'
gem 'mongoid_search', github: 'mauriciozaffari/mongoid_search', branch: 'master'

# For serializer implementation
gem 'activemodel-serializers-xml' 
gem 'active_model_serializers'

# For Serverside Pagination
gem 'will_paginate', '~> 3.1.0'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors'

# For soft delete
#gem 'acts_as_paranoid', '~> 0.6.0'


# for aws-xray
gem 'aws-xray-sdk', require: ['aws-xray-sdk/facets/rails/railtie']
gem 'oj', platform: :mri
gem 'jrjackson', platform: :jruby

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  gem 'bullet'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
